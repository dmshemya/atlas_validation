#ifndef __TrackHelperTools_h
#define __TrackHelperTools_h

#include <string>
#include <vector>
#include <TLorentzVector.h>
#include <TClonesArray.h>
#include <iostream>

#include "BaseClass.h"

using namespace std;

namespace TrackHelperTools
{
	//int getType(int barcode, int pdg, int nparent, int nparent_pdg);
	int getTypeTruth(int barcode, int pdg, int status, float charge);
	int getTypeReco(int barcode, int pdg, int status, float charge, float mc_prob, float mc_prob_cut);
	double DeltaR(double phi1, double eta1, double phi2, double eta2);
	EL::StatusCode SetCutLevel(InDet::InDetTrackSelectionTool *trackSelectionTool, string cutlevel);
	void SetupBinning(Int_t scheme, string variable, Double_t array[1000], Int_t &num);
}
#endif
