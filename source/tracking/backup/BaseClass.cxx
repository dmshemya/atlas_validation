#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "tracking_5TeV/BaseClass.h"

//#include "tracking_XeXe_xAOD/PbPbFragmentation.h"
//#include "tracking_XeXe_xAOD/PbPbFFShape.h"
//#include "tracking_XeXe_xAOD/SEBCorrectorTool.h"
//#include "pPbCentrality/pPbMinBiasUtil.h"

//#include "TriggerDef.cxx"

/*


#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODJet/JetContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include <TFile.h>
#include <TSystem.h>
*/

// this is needed to distribute the algorithm to the workers
ClassImp(BaseClass)


/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )			\
  do {							\
    if( ! EXP.isSuccess() ) {				\
      Error( CONTEXT,					\
	     XAOD_MESSAGE( "Failed to execute: %s" ),	\
	     #EXP );					\
      return EL::StatusCode::FAILURE;			\
    }							\
  } while( false )


BaseClass :: BaseClass ()
{
  
  
  
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize(). 
}

BaseClass :: BaseClass (const BaseClass& base)  {
	
	_data_switch = base._data_switch;
	_dataset = base._dataset;
	_isMB = base._isMB;
	_isHerwig = base._isHerwig;
	_jet_radius = base._jet_radius;
	_trkptBkgrThreshold = base._trkptBkgrThreshold;
	_reco_jet_collection = base._reco_jet_collection;
	_test_reco_jet_collection = base._test_reco_jet_collection;
	_truth_jet_collection=base._truth_jet_collection;
	_GRL=base._GRL;
	_cut_level=base._cut_level;
	_dR_max = base._dR_max;
	_centrality_scheme=base._centrality_scheme;
	_Run_Number=base._Run_Number;
	_truth_only = base._truth_only;
	_dR_truth_matching = base._dR_truth_matching;
	_trk_selection = base._trk_selection;
	_outputName=base._outputName;
	_pTtrkCut=base._pTtrkCut;
	_pTjetCut=base._pTjetCut;
	_truthpTjetCut=base._truthpTjetCut;
	_mcProbCut=base._mcProbCut;
	_doClusters=base._doClusters;
	_doSlimTree=base._doSlimTree;
	_doTinyTree=base._doTinyTree;
	_doForward=base._doForward;
	_pt_iso=base._pt_iso;
	_JERBalancecut=base._JERBalancecut;
	_applyReweighting=base._applyReweighting;
	_jetptBkgrThreshold=base._jetptBkgrThreshold;
	_uncert_index=base._uncert_index;
	_useCharge=base._useCharge;
	_doPileupRejection=base._doPileupRejection;
	_correctTrackpT=base._correctTrackpT;
	_eff_jety=base._eff_jety;
	_UseAltzDef=base._UseAltzDef;
	_doFJR=base._doFJR;
}


void BaseClass::SetTrigger_chains()
{
	// tracking_5TeV: 3,4,6
	
	
	if (_dataset == 3){
	// pp 2015, 5.02TeV
		if(_isMB)
		{
			_nTriggers=1;
			trigger_chains.push_back("HLT_mb_sptrk");
			
			trigger_thresholds.push_back(0);
			
			jet_pt_trig.resize(_nTriggers);
			(jet_pt_trig[0]).push_back(0); (jet_pt_trig[0]).push_back(10000.);
		}
		else if (!_doForward)
		{
			_nTriggers=7;
			_trigger_collection="a4tcemsubjesFS";
			trigger_chains.push_back("HLT_j20");
			trigger_chains.push_back("HLT_j30_L1TE5");
			trigger_chains.push_back("HLT_j40_L1TE10");
            trigger_chains.push_back("HLT_j50_L1J12");
			trigger_chains.push_back("HLT_j60_L1J15");
			trigger_chains.push_back("HLT_j75_L1J20");
			trigger_chains.push_back("HLT_j85");
           
			trigger_thresholds.push_back(20);
			trigger_thresholds.push_back(30);
			trigger_thresholds.push_back(40);
			trigger_thresholds.push_back(50);
			trigger_thresholds.push_back(60);
			trigger_thresholds.push_back(75);
			trigger_thresholds.push_back(85);
		
			jet_pt_trig.resize(_nTriggers);

			(jet_pt_trig[0]).push_back(26.0); (jet_pt_trig[0]).push_back(35.0);
			(jet_pt_trig[1]).push_back(35.0); (jet_pt_trig[1]).push_back(44.5);
			(jet_pt_trig[2]).push_back(44.5); (jet_pt_trig[2]).push_back(59.0);
			(jet_pt_trig[3]).push_back(59.0); (jet_pt_trig[3]).push_back(70.0);
			(jet_pt_trig[4]).push_back(70.0); (jet_pt_trig[4]).push_back(79.0);
			(jet_pt_trig[5]).push_back(79.0); (jet_pt_trig[5]).push_back(89.0);
			(jet_pt_trig[6]).push_back(89.0); (jet_pt_trig[6]).push_back(10000.);	
		}
		else
		{
			// this shouldn't be needed
			
			std::cout<<"error in BaseClass.cxx: We don't do forward jets"<<std::endl;
			_nTriggers=0;
			
		/*	_nTriggers=6;
			_trigger_collection="a4tcemsubjesFS";
			trigger_chains.push_back("HLT_j10_320eta490");
			trigger_chains.push_back("HLT_j15_320eta490");
			trigger_chains.push_back("HLT_j25_320eta490");
			trigger_chains.push_back("HLT_j35_320eta490");
			trigger_chains.push_back("HLT_j45_320eta490");
			trigger_chains.push_back("HLT_j55_320eta490");
		
			trigger_thresholds.push_back(10);
			trigger_thresholds.push_back(15);
			trigger_thresholds.push_back(25);
			trigger_thresholds.push_back(35);
			trigger_thresholds.push_back(45);
			trigger_thresholds.push_back(55);
		
		
			jet_pt_trig.resize(_nTriggers);
			(jet_pt_trig[0]).push_back(20); (jet_pt_trig[0]).push_back(25);
			(jet_pt_trig[1]).push_back(25); (jet_pt_trig[1]).push_back(35);
			(jet_pt_trig[2]).push_back(35); (jet_pt_trig[2]).push_back(45);
			(jet_pt_trig[3]).push_back(45); (jet_pt_trig[3]).push_back(55);
			(jet_pt_trig[4]).push_back(55); (jet_pt_trig[4]).push_back(65);
			(jet_pt_trig[5]).push_back(65); (jet_pt_trig[5]).push_back(10000.);	*/
		}
	}
	
	//PbPb 2015, 5.02TeV
	if (_dataset == 4){
		//Read prescales sets
		//Order of Y bins starting from bin #3: "HLT_j40_ion_L1TE20", "HLT_j50_ion_L1TE20", "HLT_j60_ion_L1TE50", "HLT_j75_ion_L1TE50", "HLT_j100_ion_L1TE50","HLT_j20", "HLT_j30_L1TE5", "HLT_j40_L1TE10",  "HLT_j50_L1J12","HLT_j60_L1J15","HLT_j75_L1J20","HLT_j85","HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50","HLT_noalg_mb_L1TE50","HLT_mb_sptrk"]
		cout << "Setting triggers....";
		//First trigger for PbPb FF
		_first_trigger = 1; // <=> j40
		
		
		if(_isMB)
		{
		/*	if(_isOverlay)
			{
				_nTriggers=4;
				_trigger_collection="a4ionemsubjesFS";
				trigger_chains.push_back("HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_OVERLAY");
				trigger_chains.push_back("HLT_noalg_L1TE50_OVERLAY");
				trigger_chains.push_back("HLT_noalg_mb_L1TE1500.0ETA49_OVERLAY");
				trigger_chains.push_back("HLT_noalg_mb_L1TE6500.0ETA49_OVERLAY");
					
				trigger_thresholds.push_back(0);
				trigger_thresholds.push_back(0);
				trigger_thresholds.push_back(0);
				trigger_thresholds.push_back(0);
			
				trigger_PS.push_back(1.);
				trigger_PS.push_back(1.);
				trigger_PS.push_back(1.);
				trigger_PS.push_back(1.);
			
				jet_pt_trig.resize(_nTriggers);
				(jet_pt_trig[0]).push_back(0); (jet_pt_trig[0]).push_back(10000.);
				(jet_pt_trig[1]).push_back(0); (jet_pt_trig[1]).push_back(10000.);
				(jet_pt_trig[2]).push_back(0); (jet_pt_trig[2]).push_back(10000.);
				(jet_pt_trig[3]).push_back(0); (jet_pt_trig[3]).push_back(10000.);
			}
			else */
			{
				_nTriggers=2;
				_trigger_collection="a4ionemsubjesFS";
				trigger_chains.push_back("HLT_noalg_mb_L1TE50");
				trigger_chains.push_back("HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50");
					
				trigger_thresholds.push_back(0);
				trigger_thresholds.push_back(0);
			
				jet_pt_trig.resize(_nTriggers);
				(jet_pt_trig[0]).push_back(0); (jet_pt_trig[0]).push_back(10000.);
				(jet_pt_trig[1]).push_back(0); (jet_pt_trig[1]).push_back(10000.);
			}
		}
		else
		{
			_nTriggers=5;
			_trigger_collection="a4ionemsubjesFS";
			trigger_chains.push_back("HLT_j30_ion_L1TE20");//0
			trigger_chains.push_back("HLT_j40_ion_L1TE20");//1
			trigger_chains.push_back("HLT_j50_ion_L1TE20");//2
			trigger_chains.push_back("HLT_j60_ion_L1TE50");//3
			trigger_chains.push_back("HLT_j75_ion_L1TE50");//4
			//trigger_chains.push_back("HLT_j100_ion_L1TE50");//5
					
			trigger_thresholds.push_back(30);
			trigger_thresholds.push_back(40);
			trigger_thresholds.push_back(50);
			trigger_thresholds.push_back(60);
			trigger_thresholds.push_back(75);
			//trigger_thresholds.push_back(100);
			
			
			jet_pt_trig.resize(_nTriggers);
			(jet_pt_trig[0]).push_back(45);   (jet_pt_trig[0]).push_back(58); //TODO: need to be reevaluated
			(jet_pt_trig[1]).push_back(58); (jet_pt_trig[1]).push_back(68); 
			(jet_pt_trig[2]).push_back(68); (jet_pt_trig[2]).push_back(82); 
			(jet_pt_trig[3]).push_back(82); (jet_pt_trig[3]).push_back(91); 
			(jet_pt_trig[4]).push_back(91); (jet_pt_trig[4]).push_back(10000.);
			//(jet_pt_trig[5]).push_back(116.); (jet_pt_trig[5]).push_back(10000.);
		}
	}
	
	//pPb @ 8TeV
	if (_dataset == 5){
		
		cout << "Setting triggers....";
		_first_trigger = 1;
		
		
		if(_isMB)
		{
			_nTriggers=1;
			_trigger_collection="a4ionemsubjesFS";
			trigger_chains.push_back("HLT_mb_sptrk_L1MBTS_1");
					
			trigger_thresholds.push_back(0);
			
			jet_pt_trig.resize(_nTriggers);
			(jet_pt_trig[0]).push_back(0); (jet_pt_trig[0]).push_back(10000.);
		}
		else
		{
			_nTriggers=10;
			_trigger_collection="a4ionemsubjesFS";
			// 313063-313603
			trigger_chains.push_back("HLT_j40_ion_L1J5");//0
			trigger_chains.push_back("HLT_j50_ion_L1J10");//1
			trigger_chains.push_back("HLT_j60_ion_L1J20");//2
			trigger_chains.push_back("HLT_j75_ion_L1J20");//3
			trigger_chains.push_back("HLT_j100_ion_L1J20");//4
			//313629-314170
			trigger_chains.push_back("HLT_j40_L1J5");//1
			trigger_chains.push_back("HLT_j50_L1J10");//2
			trigger_chains.push_back("HLT_j60");//3
			trigger_chains.push_back("HLT_j75_L1J20");//4
			trigger_chains.push_back("HLT_j100_L1J20");//5
			
			
			trigger_thresholds.push_back(41);
			trigger_thresholds.push_back(51);
			trigger_thresholds.push_back(61);
			trigger_thresholds.push_back(76);
			trigger_thresholds.push_back(101);
			
			trigger_thresholds.push_back(40);
			trigger_thresholds.push_back(50);
			trigger_thresholds.push_back(60);
			trigger_thresholds.push_back(75);
			trigger_thresholds.push_back(100);
			
			
			jet_pt_trig.resize(_nTriggers);
			//TODO: these ranges are basically crap, need to be changed for any real analysis
			(jet_pt_trig[0]).push_back(45);  (jet_pt_trig[0]).push_back(55); 
			(jet_pt_trig[1]).push_back(55);  (jet_pt_trig[1]).push_back(65); 
			(jet_pt_trig[2]).push_back(65);  (jet_pt_trig[2]).push_back(75); 
			(jet_pt_trig[3]).push_back(75);  (jet_pt_trig[3]).push_back(110); 
			(jet_pt_trig[4]).push_back(110); (jet_pt_trig[4]).push_back(10000.);
			
			(jet_pt_trig[5]).push_back(45);  (jet_pt_trig[5]).push_back(55); 
			(jet_pt_trig[6]).push_back(55);  (jet_pt_trig[6]).push_back(65); 
			(jet_pt_trig[7]).push_back(65);  (jet_pt_trig[7]).push_back(75); 
			(jet_pt_trig[8]).push_back(75);  (jet_pt_trig[8]).push_back(110); 
			(jet_pt_trig[9]).push_back(110); (jet_pt_trig[9]).push_back(10000.);
		}
	}
	
	// XeXe @ 5TeV
	if (_dataset == 6){
		
		cout << "Setting triggers....";
		_first_trigger = 1;
		
		
		if(_isMB)
		{
			_nTriggers=4;
			_trigger_collection="a4ionemsubjesFS";
			trigger_chains.push_back("HLT_noalg_mb_L1TE4");
			trigger_chains.push_back("HLT_mb_sptrk_L1VTE4");
			trigger_chains.push_back("HLT_mb_sptrk");
			trigger_chains.push_back("HLT_noalg_mb_L1RD0_FILLED");
			
			trigger_thresholds.push_back(0);
			trigger_thresholds.push_back(0);
			trigger_thresholds.push_back(0);
			trigger_thresholds.push_back(0);
			
			jet_pt_trig.resize(_nTriggers);
			(jet_pt_trig[0]).push_back(0); (jet_pt_trig[0]).push_back(10000.);
			(jet_pt_trig[1]).push_back(0); (jet_pt_trig[1]).push_back(10000.);
			(jet_pt_trig[2]).push_back(0); (jet_pt_trig[2]).push_back(10000.);
			(jet_pt_trig[3]).push_back(0); (jet_pt_trig[3]).push_back(10000.);
		}
		else
		{
			std::cout<<"error in BaseClass.cxx: We don't do jet triggers in XeXe"<<std::endl;
			_nTriggers=0;
		}
	}
	
}

void BaseClass::SetTrigger_hist(TH2D* h){
	for(int i=1; i<=h->GetNbinsX();i++){
		//h->GetXaxis()->SetBinLabel(i,Form("j%.0f",trigger_thresholds.at(i-1)));
		h->GetXaxis()->SetBinLabel(i,Form("%s",trigger_chains.at(i-1).c_str()) );
	}
}


