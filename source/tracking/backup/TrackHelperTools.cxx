#include "tracking_5TeV/TrackHelperTools.h"


#define EL_RETURN_CHECK( CONTEXT, EXP )			\
do {							\
if( ! EXP.isSuccess() ) {				\
Error( CONTEXT,					\
XAOD_MESSAGE( "Failed to execute: %s" ),	\
#EXP );					\
return EL::StatusCode::FAILURE;			\
}							\
} while( false )

using namespace std;


int TrackHelperTools::getTypeTruth(int barcode, int pdg, int status, float charge)
{
	// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TrackingCPMoriond2016#Truth_definitions
	// 0 - fake (should not happened) , 1 - truth primary, 2 - truth secondary, 3 - truth primary out-of-phase-space, 4 - truth secondary neutral, 5 - truth primary strange baryons
	
	if(0<barcode && barcode<200000)
	{
		if(fabs(pdg)==3112 || fabs(pdg)==3222 || fabs(pdg)==3312 || fabs(pdg)==3334) return 5;
		else
		{
			//if(status==1 && fabs(charge)>0.5) return 1;
			if(status==1) return 1;
			else return 3;
		}
	}
	
	if(200000<=barcode)
	{
		if(fabs(charge)>0.5) return 2;
		else return 4;
	}
	
	return 0; // to be safe
}


int TrackHelperTools::getTypeReco(int barcode, int pdg, int status, float charge, float mc_prob, float mc_prob_cut)
{
	// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TrackingCPMoriond2016#Truth_definitions
	// 0 - fake, 1 - primary, 2 - secondary, 3 - primary out-of-phase-space, 4 - secondary neutral, 5 - truth primary strange baryons
	
	if(barcode<=0 || mc_prob<mc_prob_cut) return 0;
	else return getTypeTruth(barcode, pdg, status, charge);
	
	
	return 0; // to be safe
}


double TrackHelperTools::DeltaR(double phi1, double eta1, double phi2, double eta2)
{
  double deta=eta1-eta2;
  double dphi=phi1-phi2;
  while (dphi < -TMath::Pi() ) dphi += 2*TMath::Pi();
  while (dphi >  TMath::Pi() ) dphi -= 2*TMath::Pi();
  return std::sqrt(deta*deta+dphi*dphi);
}


EL::StatusCode TrackHelperTools::SetCutLevel(InDet::InDetTrackSelectionTool *trackSelectionTool, string cutlevel)
{

	if (!cutlevel.std::string::compare("HITight"))
	{
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("CutLevel","HITight"));
		return EL::StatusCode::SUCCESS;
	}

	else if (!cutlevel.std::string::compare("ppTight"))
	{
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("CutLevel","TightPrimary"));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxZ0SinTheta",1.0));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("minPt",1.0));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxNSiSharedModules",100));
		return EL::StatusCode::SUCCESS;
	}

	else if (!cutlevel.std::string::compare("ppTight_manual"))
	{
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("CutLevel","NoCut"));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxAbsEta",2.5));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("minNSiHits", 9));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxNSiHoles", 2));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxNPixelHoles", 0));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("minEtaForStrictNSiHitsCut", 1.65));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("minNSiHitsAboveEtaCutoff", 11));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("useMinBiasInnermostLayersCut",1)); // s timhle asi projdou tracky, ktere nemaji hity v IBL ani v BL a ktere nemaji expected hity v BL ani v IBL; podle me takove
		return EL::StatusCode::SUCCESS;
	}

	else if (!cutlevel.std::string::compare("ppTight_tight"))
	{
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("CutLevel","TightPrimary"));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxZ0SinTheta",1.0));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("minPt",1.0));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxD0overSigmaD0",3.0));
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("maxZ0SinThetaoverSigmaZ0SinTheta",3.0));
		return EL::StatusCode::SUCCESS;
	}

	else if (!cutlevel.std::string::compare("NoCuts"))
	{
		EL_RETURN_CHECK("initialize()",trackSelectionTool->setProperty("CutLevel","NoCut"));
		return EL::StatusCode::SUCCESS;
	}


	else cout << "****TRACK SELECTOR TOOL NOT INITIALIZED****" << endl;

	return EL::StatusCode::SUCCESS;

}



void TrackHelperTools::SetupBinning(Int_t scheme, string variable, Double_t array[1000], Int_t &num)
{
   Float_t a, c;
   Int_t k;

   if ((scheme==0) && (variable=='z'))
     {num = 20;		
      array[0] = 0.005;// to overflow
      c = 0.01; 	
      a = pow( (1./c), 1./num );
      k = 1; //1 to overflow
      printf("... z-binning : ");
      for (int i=0; i<=num+1; i++)
        {Float_t value = c * pow( a, i );
         array[k] = value;
         printf("%.4f, ", array[k]);
         k++;
        }
      num = k-1; 
      cout << " ... " << num << endl;
     }
	if ((scheme==0) && (variable=="pt-trk"))
	{ num = 29;
		// old double bins[18] = {0.50, 0.63, 1.00, 1.58, 2.51, 3.98, 6.31, 10.00, 15.85, 25.12, 39.81, 63.09, 100.00, 125.89, 158.49, 199.53, 314.98, 419.98};
		double bins[30] = {0.1000, 0.1333, 0.1778, 0.2370, 0.3160, 0.4214, 0.5619, 0.9, 1.0, 1.3318, 1.7758, 2.3677, 3.1569, 4.2092, 5.6123, 7.4831, 9.9775, 13.3033, 17.7377, 23.6503, 31.5337, 42.0449, 56.0599, 74.7466, 99.6621, 132.8828, 177.1771, 236.2361, 314.9815, 419.9753};
		printf("\n... pt-trk-binning rebbin : ");
		for (int i=0; i<=num; i++)
		{
			array[i]=bins[i];
			printf("%.2f, ", array[i]);
		}
		cout << " ... " << num << endl;
	}
	
	if ((scheme==0) && (variable=="pt-trk-sum"))
	{ num = 40;
		// old double bins[18] = {0.50, 0.63, 1.00, 1.58, 2.51, 3.98, 6.31, 10.00, 15.85, 25.12, 39.81, 63.09, 100.00, 125.89, 158.49, 199.53, 314.98, 419.98};
		double bins[41] = {0.1000, 0.1333, 0.1778, 0.2370, 0.3160, 0.4214, 0.5619, 0.9, 1.0, 1.3318, 1.7758, 2.3677, 3.1569, 4.2092, 5. ,5.6123, 6.2 ,7.4831, 8.3 ,9.9775, 11. ,13.3033, 14.5 , 17.7377, 20. , 23.6503, 26. ,31.5337, 35. ,42.0449, 46.0, 56.0599, 64., 74.7466, 85. , 99.6621, 132.8828, 177.1771, 236.2361, 314.9815, 419.9753};
		printf("\n... pt-trk-binning rebbin : ");
		for (int i=0; i<=num; i++)
		{
			array[i]=bins[i];
			printf("%.2f, ", array[i]);
		}
		cout << " ... " << num << endl;
	}

	if ((scheme==0) && (variable=="pt-jet-rebin"))
	{num = 14;
		//def
		array[0] = 20; array[1] = 32;array[2] = 45;array[3] = 60;array[4] = 80;array[5] = 110;array[6] = 160;array[7] = 210;array[8] = 260;array[9] = 310;array[10] = 400;array[11] = 500; array[12] = 600; array[13] = 800; array[14] = 1000;
		//array[0] = 32; array[1] = 40;array[2] = 45;array[3] = 60;array[4] = 80;array[5] = 110;array[6] = 160;array[7] = 210;array[8] = 260;array[9] = 310;array[10] = 400;array[11] = 500; array[12] = 600; array[13] = 800; array[14] = 1000;
		printf("... pt-jet-binning : ");
		//num = 100;
		//Float_t value=0;
		for (int i=0; i<=num; i++)
		{
			//array[i] = value;
			//value = value + 2;
			printf("%.0f, ", array[i]);
		}
		cout << " ... " << num << endl;
	}
	
	if ((scheme==0) && (variable=="pt-jet-PbPb"))
	{num = 16;
		//def
		//double bins[13]={63.096, 80.0, 100.000, 125.892,  158.488,  199.525,  251.186,  316.224,  398.101,  501.178,  630.944, 794.308, 999.970};
		//double bins[17]={25.119, 31.623, 40.0, 50.119, 63.096, 79.433, 100.000, 125.892,  158.488,  199.525,  251.186,  316.224,  398.101,  500.,  630.944, 794.308, 999.970};
		double bins[17]={25.119, 31.623, 40.0, 50.119, 63.096, 82., 100.000, 125.892,  158.488,  199.525,  251.186,  316.224,  398.101,  500.,  630.944, 794.308, 999.970};
		
		
		// Binning changed from 501.178 to 500 GeV
		//double bins[12]={80.0, 100.000, 125.892,  158.488,  199.525,  251.186,  316.224,  398.101,  501.178,  630.944, 794.308, 999.970};
		printf("... pt-jet-binning : ");
		//num = 100;
		//Float_t value=0;
		for (int i=0; i<=num; i++)
		{
			array[i]=bins[i];
			//value = value + 2;
			printf("%.0f, ", array[i]);
		}
		cout << " ... " << num << endl;
	}

	if ((scheme==0) && (variable=="z-rebin"))
	{ num = 19;
		//double bins[17] = {0.0050, 0.0063, 0.0079, 0.0100, 0.0158, 0.0251, 0.0398, 0.0631, 0.1000, 0.1585, 0.2512, 0.3981, 0.6310, 1.0000, 1.2589, 1.5849, 1.9953};
		double bins[20] =      {0.0010, 0.0015, 0.0022, 0.0032, 0.0046, 0.0068, 0.0100, 0.0158, 0.0251, 0.0398, 0.0631, 0.1000, 0.1585, 0.2512, 0.3981, 0.6310, 1.0000, 1.2589, 1.5849, 1.9953};
		printf("\n... z-binning rebbin: ");
		for (int i=0; i<=num; i++)
		{
			array[i]=bins[i];
			printf("%.4f, ", array[i]);
		}
		cout << " ... " << num << endl;
	}

	if ((scheme==0) && (variable=="pt-trk-rebin"))
	{ num = 16;
		// old double bins[18] = {0.50, 0.63, 1.00, 1.58, 2.51, 3.98, 6.31, 10.00, 15.85, 25.12, 39.81, 63.09, 100.00, 125.89, 158.49, 199.53, 314.98, 419.98};
		double bins[17] = {0.50, 0.9, 1.00, 1.58, 2.51, 3.98, 6.31, 10.00, 15.85, 25.12, 39.81, 63.09, 100.00, 158.49, 251.18, 398.11, 630.96};
		printf("\n... pt-trk-binning rebbin : ");
		for (int i=0; i<=num; i++)
		{
			array[i]=bins[i];
			printf("%.2f, ", array[i]);
		}
		cout << " ... " << num << endl;
	}

	if ((scheme==0) && (variable=="pt-trk-shape"))
	{
//		num = 9;
//		double bins[10] = {0.50, 0.63, 1.00, 1.58, 2.51, 3.98, 6.31, 15.85, 39.81, 100.00};

		num = 10;
		double bins[11] = {0.50, 0.63, 1.00, 1.58, 2.51, 3.98, 6.31, 10.00, 25.12, 63.09, 158.49};

		printf("\n... pt-trk-binning for shape : ");
		for (int i=0; i<=num; i++)
		{
			array[i]=bins[i];
			printf("%.2f, ", array[i]);
		}
		cout << " ... " << num << endl;
	}

   if ((scheme==0) && (variable=="pt-jet"))
     {/*num = 100;
      array[0] = 0;
      k = 0;
      printf("\n... pt-jet-binning : ");
      for (int i=0; i<=num; i++)
        {Float_t value = i*5;
         array[k] = value;
         printf("%.4f, ", array[k]);
         k++;
        }
      num = k-1;
      cout << " ... \n" << num << endl;*/
      num = 14;
      array[0] = 10; array[1] = 32;array[2] = 45;array[3] = 60;array[4] = 80;array[5] = 110;array[6] = 160;array[7] = 210;array[8] = 260;array[9] = 310;array[10] = 400;array[11] = 500;array[12] = 600;array[13] = 800;array[14] = 1000;
      printf("\n... pt-jet-binning : ");
      for (int i=0; i<=num; i++)
        {
         printf("%.0f, ", array[i]);
        }
       cout << " ... \n" << num << endl;
     } 

   if ((scheme==0) && (variable=="eta-jet"))
     {
      printf("\n... eta-jet-binning : four simple bins ");
      array[0] = 0;
      array[1] = 0.3;
      array[2] = 0.8;
      array[3] = 1.2;
      array[4] = 2.1;
      num = 4;
     }
    if ((scheme==0) && (variable=="eta-trk"))
     {
      printf("\n... eta-trk-binning : four simple bins ");
      num = 25;
      Float_t value=-2.5; 
      for (int i=0; i<=num; i++)
        {
         array[i] = value;
         printf("%.4f, ", array[i]);
         value = value + 0.1;
        }
     }
     if ((scheme==0) && (variable=="phi-trk"))
     {
      printf("\n... phi-trk-binning : four simple bins ");
      num = 32;
      Float_t value=-TMath::Pi(); 
      for (int i=0; i<=num; i++)
        {
         array[i] = value;
         printf("%.4f, ", array[i]);
         value = value + 0.1;
        }
     }
     if ((scheme==0) && (variable=="density"))
     {
      printf("\n... density-binning : four simple bins ");
      num = 50;
      Float_t value=0; 
      for (int i=0; i<=num; i++)
        {
         array[i] = value;
         printf("%.4f, ", array[i]);
         value = value + 1;
        }
     }
     if ((scheme==0) && (variable=="hits"))
     {
      printf("\n... hits-binning : four simple bins ");
      num = 100;
      Float_t value=0; 
      for (int i=0; i<=num; i++)
        {
         array[i] = value;
         printf("%.4f, ", array[i]);
         value = value + 5;
        }
     }
     if ((scheme==0) && (variable=="hits_fine"))
     {
      printf("\n... fine-hits-binning : four simple bins ");
      num = 20;
      Float_t value=0; 
      for (int i=0; i<=num; i++)
        {
         array[i] = value;
         printf("%.4f, ", array[i]);
         value = value + 1;
        }
     }
     if ((scheme==0) && (variable=="d0z0"))
     {
      printf("\n... d0zo0: four simple bins ");
      num = 300;
      Float_t value=-1.5; 
      for (int i=0; i<=num; i++)
        {
         array[i] = value;
         printf("%.4f, ", array[i]);
         value = value + 0.01;
        }
     }
     if ((scheme==0) && (variable=="dR"))
     {
      printf("\n... dR : four simple bins ");
      num = 15;
      //Float_t value=0; 
	  array[0]=0.;array[1]=0.001;array[2]=0.002;array[3]=0.004;array[4]=0.006;array[5]=0.008;array[6]=0.01;array[7]=0.015;array[8]=0.02;array[9]=0.03;array[10]=0.04;array[11]=0.05;array[12]=0.07;array[13]=0.1;array[14]=0.2;;array[15]=0.4;
     }
     
     if ((scheme==0) && (variable=="dR-c"))
     {
      printf("\n... density-binning : four simple bins ");
      num = 9;
      //Float_t value=0; 
	  array[0]=0.;array[1]=0.01;array[2]=0.02;array[3]=0.05;array[4]=0.1;array[5]=0.5;array[6]=1;array[7]=2;array[8]=5;array[9]=10;
     }
     
     if ((scheme==0) && (variable=="MC-prob"))
     {
      printf("\n... MC probability binning ");
      num = 10;
      //Float_t value=0; 
	  array[0]=0.0;array[1]=0.1;array[2]=0.2;array[3]=0.3;array[4]=0.4;array[5]=0.5;array[6]=0.6;array[7]=0.7;array[8]=0.8;array[9]=0.9;array[10]=1.0;
      printf("\n... MC-prob-binning : ");
      for (int i=0; i<=num; i++)
        {
         printf("%.0f, ", array[i]);
        }
       cout << " ... \n" << num << endl;	
     }
     if ((scheme==0) && (variable=="dpT_fine"))
     {
      printf("\n... delta pT bins");
      num = 21;
      //Float_t value=0; 
	  array[0]=-1.;array[1]=-0.1;array[2]=-0.05;array[3]=-0.03;array[4]=-0.02;array[5]=-0.01;array[6]=-0.005;array[7]=-0.001;array[8]=-0.0005;array[9]=-0.0002;array[10]=-0.0001;array[11]=0.0;array[12]=0.0001;array[13]=0.0002;array[14]=0.0005;array[15]=0.001;array[16]=0.005;array[17]=0.01;array[18]=0.02;array[19]=0.03;array[20]=0.05;array[21]=0.1;array[21]=1;
      printf("\n... delta pt-binning : ");
      for (int i=0; i<=num; i++)
        {
         printf("%.0f, ", array[i]);
        }
       cout << " ... \n" << num << endl;	
     }
     if ((scheme==0) && (variable=="dpT"))
     {
      printf("\n... delta pT bins ");
      num = 14;
      //Float_t value=0; 
	  array[0]=-100;array[1]=-10.;array[2]=-1.;array[3]=-0.1;array[4]=-0.01;array[5]=-0.005;array[6]=-0.001;array[7]=0.0;array[8]=0.001;array[9]=0.005;array[10]=0.01;array[11]=0.1;array[12]=1;array[13]=10;array[14]=100;
      printf("\n... delta pt-binning : ");
      for (int i=0; i<=num; i++)
        {
         printf("%.0f, ", array[i]);
        }
       cout << " ... \n" << num << endl;	
     }
     if ((scheme==0) && (variable=="trk_res"))
     {
      printf("\n... trk-res-binning : four simple bins ");
      num = 400;
      Float_t value=-1; 
      for (int i=0; i<=num; i++)
        {
         array[i] = value;
         printf("%.4f, ", array[i]);
         value = value + 0.005;
        }
     }

	if ((scheme==0) && (variable=="resp"))
	{
		printf("\n... resp-binning : ");
		num = 80;
		array[0] = -0.4;
//		array[0] = 0.499375;
		for (int i=1; i<=num; i++)
		{
			array[i] = array[0]+0.01*i;
//			array[i] = array[0]+i/800.;
			printf("%4.4f, ", array[i]);
		}
		cout << " ... " << num << endl;


	}

	if ((scheme==0) && (variable=="resp-fine"))
	{
		printf("\n... fine-resp-binning : ");
		num = 800;
		array[0] = -0.020-(0.06/2400);
		//		array[0] = 0.499375;
		for (int i=1; i<=num; i++)
		{
			array[i] = array[0]+(0.06/1200)*i;
			printf("%4.4f, ", array[i]);
		}
		cout << " ... " << num << endl;

	}

	if ((scheme==0) && (variable=="dr_fine"))
	{
		printf("\n... R position res-binning : ");
		num = 80;
		array[0] = 0.;
		array[num] = 0.4;
		double w = (array[num] - array[0])/num;
		//		array[0] = 0.499375;
		for (int i=1; i<=num; i++)
		{
			array[i] = array[0]+(w*i);
			printf("%4.4f, ", array[i]);
		}
		cout << " ... " << num << endl;

	}


	if ((scheme==0) && (variable=="eta-fine"))
	{
		printf("\n... fine eta binning : ");
		num = 50;
		array[0] = -2.5;
		for (int i=1; i<=num; i++)
		{
			array[i] = array[0]+i*0.1;
			printf("%4.4f, ", array[i]);
		}
		cout << " ... " << num << endl;

	}


}
