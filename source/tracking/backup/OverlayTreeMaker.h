#ifndef __OverlayTreeMaker_h
#define __OverlayTreeMaker_h

#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>

#include "EventLoop/Job.h"
#include "EventLoop/Worker.h"
#include "EventLoop/Algorithm.h"

#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "PathResolver/PathResolver.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODForward/ForwardEventInfoContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODTracking/TrackParticleContainer.h"

#define private public
#include "xAODHIEvent/HIEventShapeAuxContainer.h"
#undef private
#include "xAODHIEvent/HIEventShapeContainer.h"



//HI_<run>_<lbn>_vtx_v1.txt 
// Run Event <vtx_x> <vtx_y> <vtx_z (in mm)>
// (for instance: 287270 360362382 -0.567135 -0.488870 15.093817)

// HI_<run>_<lbn>_filter_v1.txt
// Run Event <trigpassed> <Nvtx> <mbts dt (in ns)>
// (for instance: 287270 360196256 1 1 0.821325)

class OverlayTreeMaker : public EL::Algorithm
{
	public:
	
		TTree * overlaytree; //!
	
		int run_n, lbn_n;
		float FCalET;
		int passed_pileup, multiplicity;
		int event_n;
		float vtx_x, vtx_y, vtx_z;
		int passed_HLT_mb_sp_L1MBTS_1_OVERLAY, passed_HLT_noalg_L1TE20_OVERLAY;
		int passed_gap;
		int nvtx;
		float mbts_dt;
		
		TH1F * failhisto; //!
	
		int m_eventCounter;
	
		//GRL
		GoodRunsListSelectionTool *m_grl; //!

		//Trigger tools member variables
		Trig::TrigDecisionTool *m_trigDecisionTool; //!
		TrigConf::xAODConfigTool *m_trigConfigTool; //!
		std::vector<const Trig::ChainGroup*> _chainGroup; //!
		
		OverlayTreeMaker();
 
		// these are the functions inherited from Algorithm
		virtual EL::StatusCode setupJob (EL::Job& job);
		virtual EL::StatusCode fileExecute ();
		virtual EL::StatusCode histInitialize ();
		virtual EL::StatusCode changeInput (bool firstFile);
		virtual EL::StatusCode initialize ();
		virtual EL::StatusCode execute ();
		virtual EL::StatusCode postExecute ();
		virtual EL::StatusCode finalize ();
		virtual EL::StatusCode histFinalize ();	
		
		// this is needed to distribute the algorithm to the workers
		ClassDef(OverlayTreeMaker, 1);
};

#endif
