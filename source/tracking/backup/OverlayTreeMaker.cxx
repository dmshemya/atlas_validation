#include "tracking_5TeV/OverlayTreeMaker.h"

ClassImp(OverlayTreeMaker)

#define EL_RETURN_CHECK( CONTEXT, EXP )			\
do {							\
  if( ! EXP.isSuccess() ) {				\
    Error( CONTEXT,					\
    XAOD_MESSAGE( "Failed to execute: %s" ),	\
    #EXP );					\
    return EL::StatusCode::FAILURE;			\
    }							\
    } while( false )


OverlayTreeMaker :: OverlayTreeMaker ()
{
}

EL::StatusCode OverlayTreeMaker :: setupJob (EL::Job& job)
{
	job.useXAOD ();
	EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode OverlayTreeMaker :: histInitialize ()
{   
	overlaytree = new TTree ("tree", "Overlay Tree Maker");

	overlaytree->Branch("run_n",&run_n,"run_n/I");
	overlaytree->Branch("lbn_n",&lbn_n,"lbn_n/I");
	
	overlaytree->Branch("event_n",&event_n,"event_n/I");
	overlaytree->Branch("FCalET",&FCalET,"FCalET/F");
	
	overlaytree->Branch("multiplicity",&multiplicity,"multiplicity/I");
	overlaytree->Branch("passed_pileup",&passed_pileup,"passed_pileup/I");
	
	
	overlaytree->Branch("vtx_x",&vtx_x,"vtx_x/F");
	overlaytree->Branch("vtx_y",&vtx_y,"vtx_y/F");
	overlaytree->Branch("vtx_z",&vtx_z,"vtx_z/F");
	
	overlaytree->Branch("passed_gap", &passed_gap, "passed_gap/I");
	overlaytree->Branch("passed_HLT_mb_sp_L1MBTS_1_OVERLAY", &passed_HLT_mb_sp_L1MBTS_1_OVERLAY, "passed_HLT_mb_sp_L1MBTS_1_OVERLAY/I");
	overlaytree->Branch("passed_HLT_noalg_L1TE20_OVERLAY", &passed_HLT_noalg_L1TE20_OVERLAY, "passed_HLT_noalg_L1TE20_OVERLAY/I");
	
	overlaytree->Branch("nvtx",&nvtx,"nvtx/I");
	
	overlaytree->Branch("mbts_dt",&mbts_dt,"mbts_dt/F");
	
	wk()->addOutput(overlaytree);
	
	failhisto=new TH1F("failhisto","failhisto",9,0,9);
	failhisto->GetXaxis()->SetBinLabel(1,"all");
	failhisto->GetXaxis()->SetBinLabel(2,"GRL");
	failhisto->GetXaxis()->SetBinLabel(3,"DAQ");
	failhisto->GetXaxis()->SetBinLabel(4,"no triggers");
	failhisto->GetXaxis()->SetBinLabel(5,"only 1 vtx");
	failhisto->GetXaxis()->SetBinLabel(6,"no PV");
	failhisto->GetXaxis()->SetBinLabel(7,"vtx_z");
	failhisto->GetXaxis()->SetBinLabel(8,"MBTS");
	failhisto->GetXaxis()->SetBinLabel(9,"accepted");
	wk()->addOutput(failhisto);
	
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode OverlayTreeMaker :: fileExecute ()
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode OverlayTreeMaker :: changeInput (bool firstFile)
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode OverlayTreeMaker :: initialize ()
{
	m_eventCounter = 0;
	xAOD::TEvent* event = wk()->xaodEvent();
	
	Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int
	
	m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); 
	m_trigConfigTool->msg().setLevel( MSG::ERROR ); 
	EL_RETURN_CHECK("initialize()", m_trigConfigTool->initialize() );
	ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
	m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
	m_trigDecisionTool->msg().setLevel( MSG::ERROR ); 
	EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle) );
	EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision") );
	EL_RETURN_CHECK("initialize()", m_trigDecisionTool->initialize() );
	_chainGroup.push_back(m_trigDecisionTool->getChainGroup("HLT_mb_sp_L1MBTS_1_OVERLAY"));
	_chainGroup.push_back(m_trigDecisionTool->getChainGroup("HLT_noalg_L1TE20_OVERLAY")); 
	
	// GRL
	m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool"); 
	std::string GRLfile="tracking_5TeV/data17_hi.periodAllYear_DetStatus-v97-pro21-14_PHYS_StandardGRL_All_Good.xml";
	std::vector<std::string> vecStringGRL;
	vecStringGRL.push_back(GRLfile);
	
	EL_RETURN_CHECK("initialize()", m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
	EL_RETURN_CHECK("initialize()", m_grl->setProperty("PassThrough", false));
	EL_RETURN_CHECK("initialize()", m_grl->initialize());
	
	std::cout<<" Initialization done"<<std::endl;
	return EL::StatusCode::SUCCESS;
}

//Loop over events
EL::StatusCode OverlayTreeMaker :: execute (){

	xAOD::TEvent* event = wk()->xaodEvent();
	
	// Event counter
	int statSize=1;
	if(m_eventCounter!=0)
	{
		double power=std::floor(log10(m_eventCounter));
		statSize=(int)std::pow(10.,power);
	}
	if(m_eventCounter%statSize==0) std::cout << "Event: " << m_eventCounter << std::endl;
	m_eventCounter++;
	failhisto->Fill(0.5);
		
	const xAOD::EventInfo* eventInfo = 0;
	EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));
		
	event_n = eventInfo->eventNumber();
	run_n = eventInfo->runNumber();
	lbn_n = eventInfo->lumiBlock();
	
	
	if(!m_grl->passRunLB(*eventInfo))
	{
		failhisto->Fill(1.5);
		return EL::StatusCode::SUCCESS;
	}
	
	
	if( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error) || (eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) )
	{
		failhisto->Fill(2.5);
		return EL::StatusCode::SUCCESS;
	}
	
	

	//Centrality
	const xAOD::HIEventShapeContainer* calos = 0;
	EL_RETURN_CHECK("execute()",event->retrieve( calos, "CaloSums" ));
	FCalET = 0;
	int x = 0;
	xAOD::HIEventShapeContainer::const_iterator calo_itr = calos->begin();
	xAOD::HIEventShapeContainer::const_iterator calo_end = calos->end();
	for( ; calo_itr != calo_end; ++calo_itr ) 
	{
		if (x == 5)
		{
			FCalET = ((*calo_itr)->et() * 0.001 * 0.001 );
		}
		
		x++;
	}
	
	
	passed_HLT_mb_sp_L1MBTS_1_OVERLAY = _chainGroup.at(0)->isPassed();
	passed_HLT_noalg_L1TE20_OVERLAY   = _chainGroup.at(1)->isPassed();

	if(!passed_HLT_mb_sp_L1MBTS_1_OVERLAY && !passed_HLT_noalg_L1TE20_OVERLAY)
	{
		
		failhisto->Fill(3.5);
		return EL::StatusCode::SUCCESS;
	}  
	
	
	const xAOD::VertexContainer * vertices = 0;
	const xAOD::Vertex* primaryVertex = 0;
	xAOD::VertexContainer::const_iterator vtx_itr;
	xAOD::VertexContainer::const_iterator vtx_end;
	EL_RETURN_CHECK("execute", event->retrieve( vertices, "PrimaryVertices" ));
	if(vertices->size()<2)
	{
		failhisto->Fill(4.5);
		return EL::StatusCode::SUCCESS;
	}
	
	vtx_itr = vertices->begin();
	vtx_end = vertices->end();
	for(;vtx_itr!=vtx_end;++vtx_itr)
	{
		if((*vtx_itr)->vertexType()==xAOD::VxType::PriVtx) 
		{
			primaryVertex = (*vtx_itr);
			
			vtx_x=primaryVertex->x();
			vtx_y=primaryVertex->y();
			vtx_z=primaryVertex->z();
			
			break;
		}
	}

	if(primaryVertex)
	{
		if (fabs(primaryVertex->z())>150.)
		{
			//failhisto->Fill(6.5);
			//return EL::StatusCode::SUCCESS;
		}
	}
	else
	{
		failhisto->Fill(5.5);
		return EL::StatusCode::SUCCESS;
	}
	
	nvtx=vertices->size()-1;
	
	// rapidity gap
	passed_gap = 0;
	double m_etaGap = 0.;
	double m_etaGapA = 10.;
	double m_etaGapC = 10.;
	const xAOD::CaloClusterContainer *ptrTopoCon = 0;
	EL_RETURN_CHECK("execute()", evtStore()->retrieve(ptrTopoCon,"CaloCalTopoClusters"));
	const double topoEtCut[100] = {0.0, 0.0625, 0.0875, 0.1375, 0.1625, 0.1375, 0.1375, 0.1875, 0.2125, 0.2125, 0.2125, 0.2625, 0.2625, 0.2875, 0.3125, 0.3625, 0.4375, 0.7625, 0.6375, 0.5625, 0.7125, 0.6375, 0.6375, 0.6625, 0.6375, 0.1875, 0.7375, 1.6375, 1.5625, 0.8875, 0.8875, 1.0125, 1.3625, 0.7125, 0.7875, 0.4125, 0.2125, 0.2625, 0.3625, 0.2875, 0.3125, 0.3375, 0.3625, 0.3375, 0.3625, 0.4125, 0.4625, 0.4125, 0.4125, 0.3875, 0.4125, 0.4125, 0.3875, 0.3875, 0.3875, 0.3625, 0.3375, 0.3375, 0.3375, 0.3125, 0.2875, 0.2875, 0.2625, 0.2125, 0.3375, 0.7375, 0.5125, 0.8625, 0.7875, 1.5875, 0.5125, 0.6625, 0.6125, 0.3125, 0.6125, 0.6375, 0.6625, 0.6125, 0.5875, 0.8625, 0.8375, 0.4125, 0.4875, 0.4375, 0.3875, 0.3375, 0.3125, 0.2875, 0.2875, 0.2625, 0.2375, 0.2625, 0.2375, 0.2125, 0.1875, 0.1875, 0.1625, 0.1375, 0.0375, 0.0};
	for(const auto *ptrTopo : *ptrTopoCon)
	{
		double eta = ptrTopo->eta();
		//double sg = ptrTopo->getMomentValue(xAOD::CaloCluster::CELL_SIGNIFICANCE);
		double et = ptrTopo->pt()/1000.;

		// calculating eta gap
		int iEta = int((eta+5)*10);
		if(iEta<0 || iEta>=100) continue;
		if(et<topoEtCut[iEta]) continue;
			
		m_etaGapA=std::min(5-eta,m_etaGapA);
		m_etaGapC=std::min(5+eta,m_etaGapC);
	}
	m_etaGap=std::max(m_etaGapA,m_etaGapC);
	if(m_etaGap<=2.1) passed_gap=1;


	//Tracks
	const xAOD::TrackParticleContainer* recoTracks = 0;
	multiplicity=0;
	
	if ( !event->retrieve( recoTracks, "InDetTrackParticles" ).isSuccess() )
	{
		Error("execute()", "Failed to retrieve Reconstructed Track container. Exiting." );
		return EL::StatusCode::FAILURE;
	}
		
	for (const auto& trk : *recoTracks)
	{
		if(trk->pt()<500.) continue;
		if(fabs(trk->d0())>=1.5) continue;
		if(fabs(sin(trk->theta())*(trk->z0()+trk->vz()-primaryVertex->z()))>=1.5) continue;
		if(fabs(trk->eta())>=2.5) continue;
		if(trk->auxdata<unsigned char>("numberOfPixelHits")<1) continue;
		if(trk->auxdata<unsigned char>("numberOfSCTHits")+trk->auxdata<unsigned char>("numberOfSCTDeadSensors")<6) continue;
		if(trk->auxdata<unsigned char>("expectInnermostPixelLayerHit")>0)
		{
			if(trk->auxdata<unsigned char>("numberOfInnermostPixelLayerHits")==0) continue;
		}
		else
		{
			if(trk->auxdata<unsigned char>("expectNextToInnermostPixelLayerHit")>0 && trk->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerHits")==0) continue;
		}
			
		++multiplicity;
	}
	
	if(FCalET>0.0015335*multiplicity+0.21047) passed_pileup=0; // Mingliang
	else                                      passed_pileup=1;

	
	const xAOD::ForwardEventInfoContainer * mbtsEventInfo = 0;
	EL_RETURN_CHECK("execute", event->retrieve( mbtsEventInfo, "MBTSForwardEventInfo"));
	
	xAOD::ForwardEventInfoContainer::const_iterator mbts_itr = mbtsEventInfo->begin();
	xAOD::ForwardEventInfoContainer::const_iterator mbts_end = mbtsEventInfo->end();
	
	if(mbtsEventInfo->size() != 1)
	{
		std::cout<<"OverlayTreeMaker :: execute - mbtsEventInfo has size "<<mbtsEventInfo->size()<<std::endl;
		failhisto->Fill(7.5);
		return EL::StatusCode::SUCCESS;
	}
	else
	{
		mbts_dt=(*mbts_itr)->timeDiff();
	}
	
	failhisto->Fill(8.5);
	overlaytree->Fill();
	
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode OverlayTreeMaker :: postExecute()
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode OverlayTreeMaker :: finalize()
{
	//xAOD::TEvent* event = wk()->xaodEvent();
	
	delete m_trigConfigTool;
	delete m_trigDecisionTool;
	delete m_grl;
	
	return EL::StatusCode::SUCCESS;
}
	
EL::StatusCode OverlayTreeMaker :: histFinalize ()
{  
	std::cout<<"Events = "<<m_eventCounter<<std::endl;
	return EL::StatusCode::SUCCESS;
}
