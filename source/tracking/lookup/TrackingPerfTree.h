#ifndef DimuonAna_TrackingPerfTree_H
#define DimuonAna_TrackingPerfTree_H

#include <TTree.h>
#include <TH1.h>
#include <TH2.h>
#include <vector>

#include <EventLoop/Algorithm.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>

//tools:
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

#include "JetSelectorTools/JetCleaningTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"

#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/IMuonEfficiencyScaleFactors.h"
#include "MuonSelectorTools/MuonSelectionTool.h"

//#include "PileupReweighting/PileupReweightingTool.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"

#include "IsolationSelection/IsolationSelectionTool.h"

//#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"


#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "TrigMuonMatching/TrigMuonMatching.h"

#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"

using namespace std;
using namespace CP;
using namespace xAOD;
using namespace InDet;

class TrackingPerfTree : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
 public:
  // float cutValue;
  //counters

  const xAOD::EventInfo* eventInfo;//!

  int m_numCleanEvents; //!
  int m_eventCounter; //!
  int m_passeventCounter; //!
  //tools
  GoodRunsListSelectionTool *m_grl; //!
  GoodRunsListSelectionTool *m_grl2; //!
  JetCleaningTool *m_jetCleaning; //!  
  JetCalibrationTool *m_jetCalibration; //!  
  InDetTrackSelectionTool *m_trackSelection; //!  

  JetVertexTaggerTool* pjvtag = 0;//!
  ToolHandle<IJetUpdateJvt> hjvtagup;//!

  CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; //! 
  //  CP::IPileupReweightingTool* m_PRWTool;       	//!
  //  ToolHandle<CP::IPileupReweightingTool> PRWToolHandle ; //!
  asg::AnaToolHandle<CP::IPileupReweightingTool> m_PRWTool;
  MuonSelectionTool *m_muonSelection; //!  
  IsolationSelectionTool *m_IsoSelection; //!  

  //  CP::MuonEfficiencyScaleFactors m_effi_corr; //!  
  //  CP::MuonEfficiencyScaleFactors *m_effi_corr; //!  
  //MuonEfficiencyScaleFactors *m_effi_corr; //!  
  //  CP::MuonEfficiencyScaleFactors *m_effi_corr("MuonEffTool");//!  
  
  CP::MuonEfficiencyScaleFactors * m_effi_corr;//!
  CP::SystematicSet statup;//!
  CP::SystematicSet statdown;//!
  CP::SystematicSet sysup;//!
  CP::SystematicSet sysdown;//!
  CP::SystematicSet loptstatup;//!
  CP::SystematicSet loptstatdown;//!
  CP::SystematicSet loptsysup;//!
  CP::SystematicSet loptsysdown;//!

  //  AsgElectronLikelihoodTool* m_MediumLH ;//!

  // trigger tools member variables
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!

  Trig::TrigMuonMatching *m_match_Tool;//!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  const float mumass = 0.1057;

////  TH1D *ntrkdist;//!
////  TH1D *ntrkdist_onejet;//!
////  TH1D *mudist[12];//!
////  TH1D *mudist_onejet[12];//!
////  TH2D *vtxspacing;//!
////  TH2D *vtxspacing_onejet;//!
////  //  TH2D *x_vtxspacing;//!
////  //  TH2D *y_vtxspacing;//!
////  TH2D *primary_pt2;//!
////  TH2D *secondary_pt2;//!
////  TH2D *primary_pt2_onejet;//!
////  TH2D *secondary_pt2_onejet;//!
////  //  TH2D *deltaZdist;//!
////  TH2D *sindeltaZdist[12];//!
////  TH2D *sindeltaZdist_onejet[12];//!
////  //  TH2D *sindeltaZdist_ntrk;//!

  // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree 
  std::string outputName;
  //tree and the variables that will fill it
  TTree *pairs; //!
  float RunNumber; //!
  double EventNumber; //!
  float bcid; //!
  float lbn; //!
  float averageIntPerXing; //!
  float actualIntPerXing; //!
  float mufromtool; //!

  float utps66;//!
  float utps64;//!
  float utps44;//!

  float Noftracks;//!
  float Nofbkgs;//!
  float Nvtxs;//!
  float primary_vx_z;//!
  float truth_primary_vx_z;//!
  //  float truth_Npup_vxs;//!
  float closest_pup;//!


  vector<float> *  recoeff1=0; //!
  vector<float> *  recoeff2=0; //!
  vector<float> *  recotosub1=0; //!
  vector<float> *  recotosub2=0; //!
  vector<float> *  recopt1=0; //!
  vector<float> *  recopt2=0; //!
  vector<float> *  recod01=0; //!
  vector<float> *  recod02=0; //!
  vector<float> *  recoiso1=0; //!
  vector<float> *  recoiso2=0; //!
  vector<float> *  recod0s1=0; //!
  vector<float> *  recod0s2=0; //!
  vector<float> *  recotrig1=0; //!
  vector<float> *  recotrig2=0; //!
  vector<float> *  recoutrig1=0; //!
  vector<float> *  recoutrig2=0; //!
  vector<float> *  recoeta1=0; //!
  vector<float> *  recoeta2=0; //!
  vector<float> *  recophi1=0; //!
  vector<float> *  recophi2=0; //!
  vector<float> *  recoch1=0; //!
  vector<float> *  recoch2=0; //!
  vector<float> *  pairpt=0; //!
  vector<float> *  pairy=0; //!
  vector<float> *  paireta=0; //!
  vector<float> *  pairm=0; //!
  vector<float> *  pairphi=0; //!

  vector< vector<float> > * recoerr1=0;//!
  vector< vector<float> > * recoerr2=0;//!

  vector<float> * vxs  =0; //!

  vector<float> * jet4_pt  =0; //!
  vector<float> * jet4_uncalibpt  =0; //!
  vector<float> * jet4_eta  =0; //!
  vector<float> * jet4_phi  =0; //!
  vector<float> * jet4_ntrks  =0; //!
  vector<float> * jet4_ntrks5  =0; //!
  vector<float> * jet4_ntrks6  =0; //!
  vector<float> * jet4_jvf  =0; //!

  vector<float> * trks_true  =0; //!
  vector<float> * trks_eta  =0; //!
  vector<float> * trks_phi  =0; //!
  vector<float> * trks_pt  =0; //!
  vector<float> * trks_sinthetaz0  =0; //!



  //truth stuff
  float Upspt; //!
  float Upsy; //!
  float Upsm; //!
  float Upsphi; //!
  float UpsID; //!
  float UpsParentID; //!

  float truemupt1; //!
  float truemupt2; //!
  float truemueta1; //!
  float truemueta2; //!
  float truemuphi1; //!
  float truemuphi2; //!
  float truemuch1; //!
  float truemuch2; //!

  vector<float> * truesib_recoq  =0; //!
  vector<float> * truesib_recoz0  =0; //!
  vector<float> * truesib_eta  =0; //!
  vector<float> * truesib_phi  =0; //!
  vector<float> * truesib_pt  =0; //!
  vector<float> * truesib_ID  =0; //!

  vector<float> * truetrk_recoq  =0; //!
  vector<float> * truetrk_recoz0  =0; //!
  vector<float> * truetrk_eta  =0; //!
  vector<float> * truetrk_phi  =0; //!
  vector<float> * truetrk_pt  =0; //!
  vector<float> * truetrk_charge  =0; //!
  vector<float> * truetrk_vxz  =0; //!
  vector<float> * truetrk_vxx  =0; //!
  vector<float> * truetrk_vxy  =0; //!


  // this is a standard constructor
  TrackingPerfTree ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TrackingPerfTree, 1);

  //these are custom functions
  double getdR(double eta1,double phi1,double eta2,double phi2)
  {
    double dphi = phi1-phi2;
    if(dphi>TMath::Pi())dphi=dphi-2*TMath::Pi();
    else if(dphi<-TMath::Pi())dphi=dphi+2*TMath::Pi();
    double deta = eta1-eta2;
    return sqrt(deta*deta + dphi*dphi);
  }

  int getzposbin(double zpos)
  {
    if(zpos<-150) return 0;
    if(zpos>150) return 11;
    int bin=1;
    for(int i=0;i<10;i++) if(zpos > (i*30  -120) ) bin++; 
    return bin;
  }

};

#endif

