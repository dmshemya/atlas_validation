#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DimuonAna/TrackingPerfTree.h>
#include <TSystem.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TNtuple.h>
#include <TLorentzVector.h>
#include <vector>
#include <cmath>
#include <stdio.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"

// ASG status code check
#include <AsgTools/MessageCheck.h>

// EDM includes:
#include "xAODEventInfo/EventInfo.h"

//Physics includes
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"

//#include "xAODEgamma/ElectronContainer.h"//special temo one

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigMuonMatching/TrigMuonMatching.h"

//#include "JetMomentTools/JetVertexTaggerTool.h"

#include "PATInterfaces/CorrectionCode.h" // to check the return correction code status of tools
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"

#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"

#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"

//truth

#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
// this is needed to distribute the algorithm to the workers
ClassImp(TrackingPerfTree)

#define CHECK( CONTEXT, EXP )						\
  {									\
    if (!EXP.isSuccess())						\
      Fatal( CONTEXT, "Failed to execute: %s. Exiting.", #EXP);		\
  }

TrackingPerfTree :: TrackingPerfTree ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}


EL::StatusCode TrackingPerfTree :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once) 


  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();

  //  xAOD::Init(); // call before opening first file
  //  EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
  ANA_CHECK(xAOD::Init());



  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  //Histograms
////  char histoname[512];
////  for(int i=0;i<12;i++)
////    { 
////      sprintf(histoname,"mudist_%d",i);
////      mudist[i]= new TH1D(histoname,histoname,30,0.5,30.5); 
////      sprintf(histoname,"mudist_onejet_%d",i);
////      mudist_onejet[i]= new TH1D(histoname,histoname,30,0.5,30.5); 
////      sprintf(histoname,"sindeltaZdist_%d",i);
////      sindeltaZdist[i] = new TH2D(histoname,histoname,400,-20,20,30,0.5,30.5); 
////      sprintf(histoname,"sindeltaZdist_onejet_%d",i);
////      sindeltaZdist_onejet[i] = new TH2D(histoname,histoname,400,-20,20,30,0.5,30.5); 
////
////      wk()->addOutput (sindeltaZdist[i]);
////      wk()->addOutput (mudist[i]);
////      wk()->addOutput (sindeltaZdist_onejet[i]);
////      wk()->addOutput (mudist_onejet[i]);
////    }
////  vtxspacing = new TH2D("vtxspacing","vtxspacing",500,-100,100,30.,0.5,30.5); 
////  ntrkdist = new TH1D("ntrkdist","ntrkdist",20,0,200); 
//   float xaxis[191];
//  for(int i=0;i<100;i++) xaxis[i]=i*0.1;
//  for(int i=100;i<191;i++) xaxis[i]=i-90;
//  float yaxis[31];
//  for(int i=0;i<31;i++) yaxis[i]=0.5+i;
////  primary_pt2 = new TH2D("primary_pt2","primary_pt2",240,0,120,30,0.5,30.5);
////  secondary_pt2 = new TH2D("secondary_pt2","secondary_pt2",240,0,120,30,0.5,30.5);
////  //  deltaZdist = new TH2D("deltaZdist","deltaZdist",1600,-100.,100.,30.,0.5,30.5); 
////  wk()->addOutput (ntrkdist);
////  wk()->addOutput (vtxspacing);
////  wk()->addOutput (primary_pt2);
////  wk()->addOutput (secondary_pt2);
////  //wk()->addOutput (deltaZdist);
////
////  vtxspacing_onejet = new TH2D("vtxspacing_onejet","vtxspacing_onejet",500,-100,100,30.,0.5,30.5); 
////  ntrkdist_onejet = new TH1D("ntrkdist_onejet","ntrkdist_onejet",20,0,200); 
////  primary_pt2_onejet = new TH2D("primary_pt2_onejet","primary_pt2_onejet",240,0,120,30,0.5,30.5);
////  secondary_pt2_onejet = new TH2D("secondary_pt2_onejet","secondary_pt2_onejet",240,0,120,30,0.5,30.5);
////  wk()->addOutput (ntrkdist_onejet);
////  wk()->addOutput (vtxspacing_onejet);
////  wk()->addOutput (primary_pt2_onejet);
////  wk()->addOutput (secondary_pt2_onejet);

  // get the output file, create a new TTree and connect it to that output
  // define what braches will go in that tree
  TFile *outputFile = wk()->getOutputFile (outputName);
  pairs = new TTree ("pairs", "pairs");
  pairs->SetDirectory (outputFile);
  pairs->Branch("RunNumber", &RunNumber);
  pairs->Branch("EventNumber", &EventNumber);
  pairs->Branch("lbn", &lbn);
  pairs->Branch("bcid", &bcid);
  pairs->Branch("averageIntPerXing", &averageIntPerXing);
  pairs->Branch("actualIntPerXing", &actualIntPerXing);
  pairs->Branch("mufromtool", &mufromtool);

  pairs->Branch("utps66", &utps66);
  pairs->Branch("utps64", &utps64);
  pairs->Branch("utps44", &utps44);

  pairs->Branch("Noftracks", &Noftracks);
  pairs->Branch("Nofbkgs", &Nofbkgs);
  pairs->Branch("Nvtxs", &Nvtxs);
  pairs->Branch("primary_vx_z", &primary_vx_z);
  pairs->Branch("truth_primary_vx_z", &truth_primary_vx_z);
  //  pairs->Branch("truth_Npup_vxs", &truth_Npup_vxs);
  pairs->Branch("closest_pup", &closest_pup);

  pairs->Branch("pairpt", &pairpt);
  pairs->Branch("pairy", &pairy);
  pairs->Branch("paireta", &paireta);
  pairs->Branch("pairm", &pairm);
  pairs->Branch("pairphi", &pairphi);

  pairs->Branch("recoeff1", &recoeff1);
  pairs->Branch("recotosub1", &recotosub1);
  pairs->Branch("recoerr1", &recoerr1);
  pairs->Branch("recopt1", &recopt1);
  pairs->Branch("recophi1", &recophi1);
  pairs->Branch("recoeta1", &recoeta1);
  pairs->Branch("recoch1", &recoch1);
  pairs->Branch("recod01", &recod01);
  pairs->Branch("recod0s1", &recod0s1);
  pairs->Branch("recotrig1", &recotrig1);
  pairs->Branch("recoutrig1", &recoutrig1);
  pairs->Branch("recoiso1", &recoiso1);

  pairs->Branch("recoeff2", &recoeff2);
  pairs->Branch("recotosub2", &recotosub2);
  pairs->Branch("recoerr2", &recoerr2);
  pairs->Branch("recopt2", &recopt2);
  pairs->Branch("recophi2", &recophi2);
  pairs->Branch("recoeta2", &recoeta2);
  pairs->Branch("recoch2", &recoch2);
  pairs->Branch("recod02", &recod02);
  pairs->Branch("recod0s2", &recod0s2);
  pairs->Branch("recotrig2", &recotrig2);
  pairs->Branch("recoutrig2", &recoutrig2);
  pairs->Branch("recoiso2", &recoiso2);

  pairs->Branch("vxs",&vxs);

  pairs->Branch("jet4_pt",&jet4_pt);
  pairs->Branch("jet4_uncalibpt",&jet4_uncalibpt);
  pairs->Branch("jet4_eta",&jet4_eta);
  pairs->Branch("jet4_phi",&jet4_phi);
//  pairs->Branch("jet4_ntrks",&jet4_ntrks);
//  pairs->Branch("jet4_ntrks5",&jet4_ntrks5);
//  pairs->Branch("jet4_ntrks6",&jet4_ntrks6);
  pairs->Branch("jet4_jvf",&jet4_jvf);

  pairs->Branch("trks_eta",&trks_eta);
  pairs->Branch("trks_sinthetaz0",&trks_sinthetaz0);
  pairs->Branch("trks_phi",&trks_phi);
  pairs->Branch("trks_pt",&trks_pt);
  pairs->Branch("trks_true",&trks_true);


  pairs->Branch("Upspt",  &Upspt);
  pairs->Branch("Upsy",   &Upsy);
  pairs->Branch("Upsm",  &Upsm);
  pairs->Branch("Upsphi",&Upsphi);
  pairs->Branch("UpsID",&UpsID);
  pairs->Branch("UpsParentID",&UpsParentID);

  pairs->Branch("truemupt1", &truemupt1);
  pairs->Branch("truemuphi1", &truemuphi1);
  pairs->Branch("truemueta1", &truemueta1);
  pairs->Branch("truemuch1", &truemuch1);
  //  pairs->Branch("recosfl1", &recosfl1);
  //  pairs->Branch("recosfm1", &recosfm1);
  //  pairs->Branch("recosft1", &recosft1);
  //  pairs->Branch("recostruematch1", &recotruematch1);

  pairs->Branch("truemupt2", &truemupt2);
  pairs->Branch("truemuphi2", &truemuphi2);
  pairs->Branch("truemueta2", &truemueta2);
  pairs->Branch("truemuch2", &truemuch2);
  //  pairs->Branch("recosfl2", &recosfl2);
  //  pairs->Branch("recosfm2", &recosfm2);
  //  pairs->Branch("recosft2", &recosft2);
  //  pairs->Branch("recostruematch2", &recotruematch2);

  pairs->Branch("truesib_recoq",&truesib_recoq);
  pairs->Branch("truesib_recoz0",&truesib_recoz0);
  pairs->Branch("truesib_eta",&truesib_eta);
  pairs->Branch("truesib_ID", &truesib_ID);
  pairs->Branch("truesib_phi",&truesib_phi);
  pairs->Branch("truesib_pt", &truesib_pt);

  pairs->Branch("truetrk_vxz",&truetrk_vxz);
  pairs->Branch("truetrk_vxy",&truetrk_vxy);
  pairs->Branch("truetrk_vxx",&truetrk_vxx);
  pairs->Branch("truetrk_recoq",&truetrk_recoq);
  pairs->Branch("truetrk_recoz0",&truetrk_recoz0);
  pairs->Branch("truetrk_eta",&truetrk_eta);
  pairs->Branch("truetrk_phi",&truetrk_phi);
  pairs->Branch("truetrk_charge",&truetrk_charge);
  pairs->Branch("truetrk_pt", &truetrk_pt);

//  pairs->Branch("fliptrks_pt",&fliptrks_pt);
//  pairs->Branch("fliptrks_phi",&fliptrks_phi);
//  pairs->Branch("fliptrks_eta",&fliptrks_eta);

  //  pairs->Branch("jet4_ntrk1000",&jet4_ntrk1000);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once) 

  xAOD::TEvent* event = wk()->xaodEvent();


  // GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  std::vector<std::string> vecStringGRL;
  const char* grlFilePath = "$ROOTCOREBIN/data/DimuonAna/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
  const char* fullGRLFilePath = gSystem->ExpandPathName (grlFilePath);
  vecStringGRL.push_back(fullGRLFilePath);

  ANA_CHECK(m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
  ANA_CHECK(m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK(m_grl->initialize());


  m_grl2 = new GoodRunsListSelectionTool("GoodRunsListSelectionTool2");
  std::vector<std::string> vecStringGRL2;
  grlFilePath = "$ROOTCOREBIN/data/DimuonAna/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
  fullGRLFilePath = gSystem->ExpandPathName (grlFilePath);
  vecStringGRL2.push_back(fullGRLFilePath);

  ANA_CHECK(m_grl2->setProperty( "GoodRunsListVec", vecStringGRL2));
  ANA_CHECK(m_grl2->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK(m_grl2->initialize());




  // initialize and configure the jet cleaning tool
  m_jetCleaning = new JetCleaningTool("JetCleaning");
  m_jetCleaning->msg().setLevel( MSG::DEBUG ); 
  ANA_CHECK(m_jetCleaning->setProperty( "CutLevel", "LooseBad"));
  ANA_CHECK(m_jetCleaning->setProperty("DoUgly", false));
  ANA_CHECK(m_jetCleaning->initialize());

  // initialize and configure the jet calibration tool
  const std::string name = "DimxAODAnalysis"; //string describing the current thread, for logging
  TString jetAlgo = "AntiKt4EMTopo"; //String describing your jet collection, for example AntiKt4EMTopo or AntiKt4LCTopo (see above)
  //TString config = "JES_Full2012dataset_May2014.config"; //Path to global config used to initialize the tool (see above) //old one i.e. run1
  //  TString calibSeq = "JetArea_Residual_Origin_EtaJES_GSC_Insitu"; //String describing the calibration sequence to apply (see above)
  TString config =   "JES_MC15cRecommendation_May2016.config ";
  TString calibSeq =  "JetArea_Residual_Origin_EtaJES_GSC_Insitu" ;
  bool isData = true; //bool describing if the events are data or from simulation
  m_jetCalibration = new JetCalibrationTool(name, jetAlgo, config, calibSeq, isData);  //Call the constructor. 
  ANA_CHECK(m_jetCalibration->initializeTool(name));
  
  // Configure the JVT tool.
  //JetVertexTaggerTool* pjvtag = 0;
  //  ToolHandle<IJetUpdateJvt> hjvtagup;
  pjvtag = new JetVertexTaggerTool("jvtag");
  hjvtagup = ToolHandle<IJetUpdateJvt>("jvtag");
  ANA_CHECK(pjvtag->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root"));
  ANA_CHECK(pjvtag->initialize() );
//  bool fail = false;
//  fail |= pjvtag->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root").isFailure();
//  fail |= pjvtag->initialize().isFailure();
//  
//  if ( fail ) {
//    cout << myname << "Tool initialialization failed!" << endl;
//    return 1;
//  }

//  cout<<"A"<<endl;
// Initialize and configure trigger tools

  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
  ANA_CHECK( m_trigConfigTool->initialize() );
  ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  ANA_CHECK(m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
  ANA_CHECK(m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
  ANA_CHECK(m_trigDecisionTool->initialize() );
  ToolHandle< Trig::TrigDecisionTool > m_trigDecision(m_trigDecisionTool);


  m_match_Tool = new Trig::TrigMuonMatching ("TrigMuonMatching");
  ANA_CHECK(m_match_Tool->setProperty("TriggerTool",m_trigDecision));
  ANA_CHECK(m_match_Tool->initialize());
  

/*
  xAODConfigTool configTool("xAODConfigTool");
  ToolHandle<TrigConf::ITrigConfigTool> configHandle(&configTool);
  ASG_CHECK_SA(APP_NAME,configHandle->initialize());
  
  TrigDecisionTool trigDecTool("TrigDecTool");
  ASG_CHECK_SA(APP_NAME, trigDecTool.setProperty("ConfigTool",configHandle));
  ASG_CHECK_SA(APP_NAME, trigDecTool.setProperty("TrigDecisionKey","xTrigDecision"));
  ASG_CHECK_SA(APP_NAME, trigDecTool.initialize());
  ToolHandle<Trig::TrigDecisionTool> m_trigDec(&trigDecTool);
  

*/
  
  
//  cout<<"B"<<endl;
/*
  m_PRWTool = new CP::PileupReweightingTool("PRWTool");//old way
//  asg::AnaToolHandle < CP::IPileupReweightingTool > m_PRW("CP::PileupReweightingTool/myTool");//new way matches new header include
  std::vector<std::string> vecStringPRW;
  const char* prwFilePath = "$ROOTCOREBIN/data/HIDimuon/pileup_rw.mc15c.5TeV.root";
  const char* fullPRWFilePath = gSystem->ExpandPathName (prwFilePath);
  vecStringPRW.push_back(fullPRWFilePath);  
  ANA_CHECK(asg::setProperty(m_PRWTool,"ConfigFiles",vecStringPRW));
 
  //  std::vector<std::string> m_ConfigFiles { "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root" };
  //  ANA_CHECK(asg::setProperty(m_PRWTool, "ConfigFiles", m_ConfigFiles));
  std::vector<std::string> lcalcFiles;
  //  const char* LumiFilePath = "$ROOTCOREBIN/data/DimuonAna/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root";
  const char* LumiFilePath = "$ROOTCOREBIN/data/DimuonAna/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-008.root";
  const char* fullLumiFilePath = gSystem->ExpandPathName (LumiFilePath);
  lcalcFiles.push_back(fullLumiFilePath);
  ANA_CHECK(asg::setProperty(m_PRWTool, "LumiCalcFiles", lcalcFiles));
  // ANA_CHECK(asg::setProperty(m_PRWTool, "DefaultChannel",  410000));
  ANA_CHECK(asg::setProperty(m_PRWTool,"DataScaleFactor",1./1.09));
  ANA_CHECK(asg::setProperty(m_PRWTool,"DataScaleFactorUP",1.));
  ANA_CHECK(asg::setProperty(m_PRWTool,"DataScaleFactorDOWN",1./1.18));
  ANA_CHECK(m_PRWTool->initialize());
  ToolHandle<CP::IPileupReweightingTool> PRWToolHandle = m_PRWTool; 
*/

  CHECK("Initialize()",m_PRWTool.make("CP::PileupReweightingTool/prw"));//from egamma example
  std::vector<std::string> m_ConfigFiles { "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root" };
  CHECK("Initialize()", m_PRWTool.setProperty( "ConfigFiles", m_ConfigFiles));
  std::vector<std::string> lcalcFiles;
  const char* LumiFilePath = "$ROOTCOREBIN/data/DimuonAna/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-008.root";
  const char* fullLumiFilePath = gSystem->ExpandPathName (LumiFilePath);
  lcalcFiles.push_back(fullLumiFilePath);
  CHECK("Initialize()", m_PRWTool.setProperty( "LumiCalcFiles", lcalcFiles));
  CHECK("Initialize()", m_PRWTool.setProperty( "DataScaleFactorUP", 1.0/1.0));
  CHECK("Initialize()", m_PRWTool.setProperty( "DataScaleFactor", 1.0/1.16));
  CHECK("Initialize()", m_PRWTool.setProperty( "DataScaleFactorDOWN", 1.0/1.23));
  //  CHECK("Initialize()", m_PRWTool.setProperty( "DefaultChannel", 301535));
  CHECK("Initialize()", m_PRWTool.retrieve()); 
 
//  asg::AnaToolHandle < CP::IPileupReweightingTool > m_prw_tool("CP::PileupReweightingTool/myTool");
//  // This is just a placeholder configuration for testing. Do not use these config files for your analysis!
//  std::vector<std::string> m_ConfigFiles { "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root" };
//  //ASG_CHECK_SA(APP_NAME, m_prw_tool.setProperty("ConfigFiles", m_ConfigFiles));
//  ANA_CHECK(asg::setProperty(m_prw_tool, "ConfigFiles", m_ConfigFiles));
//  std::vector<std::string> m_LumiCalcFiles { "/afs/cern.ch/atlas/project/muon/mcp/PRWFiles/ilumicalc_histograms_data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.root", "/afs/cern.ch/atlas/project/muon/mcp/PRWFiles/ilumicalc_histograms_data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.root" };
//  ANA_CHECK(asg::setProperty(m_PRWTool, "LumiCalcFiles", m_LumiCalcFiles));
//  ANA_CHECK(asg::setProperty(m_prw_tool,"DataScaleFactor",1./1.09));
//  ANA_CHECK(asg::setProperty(m_prw_tool,"DataScaleFactorUP",1.));
//  ANA_CHECK(asg::setProperty(m_prw_tool,"DataScaleFactorDOWN",1./1.18));
//  ANA_CHECK(m_PRWTool->initialize());
////  ASG_CHECK_SA(APP_NAME, m_prw_tool.setProperty("LumiCalcFiles", m_LumiCalcFiles));
////  ASG_CHECK_SA(APP_NAME, m_prw_tool.setProperty("DataScaleFactor", 1.0 / 1.09));
////  ASG_CHECK_SA(APP_NAME, m_prw_tool.setProperty("DataScaleFactorUP", 1.));
////  ASG_CHECK_SA(APP_NAME, m_prw_tool.setProperty("DataScaleFactorDOWN", 1.0 / 1.18));
//  // Initialize the PRW tool
//  //  ASG_CHECK_SA(APP_NAME, m_prw_tool.initialize());


  m_effi_corr = new CP::MuonEfficiencyScaleFactors("MuonEffTool");
  ANA_CHECK(m_effi_corr->setProperty( "WorkingPoint", "Tight"));
  //ANA_CHECK(m_effi_corr->setProperty("PileupReweightingTool", PRWToolHandle));
  ANA_CHECK(m_effi_corr->initialize());


  statup.insert(CP::SystematicVariation("MUON_EFF_STAT", 1));
  statdown.insert(CP::SystematicVariation("MUON_EFF_STAT", -1));
  sysup.insert(CP::SystematicVariation("MUON_EFF_SYS", 1));
  sysdown.insert(CP::SystematicVariation("MUON_EFF_SYS", -1));

  loptstatup.insert(CP::SystematicVariation("MUON_EFF_STAT_LOWPT", 1));
  loptstatdown.insert(CP::SystematicVariation("MUON_EFF_STAT_LOWPT", -1));
  loptsysup.insert(CP::SystematicVariation("MUON_EFF_SYS_LOWPT", 1));
  loptsysdown.insert(CP::SystematicVariation("MUON_EFF_SYS_LOWPT", -1));



  // initialize the muon calibration and smearing tool
  m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool" );
  //m_muonCalibrationAndSmearingTool->msg().setLevel( MSG::DEBUG );
  ANA_CHECK(m_muonCalibrationAndSmearingTool->initialize());
  //  cout<<"C"<<endl;

  //initialize (and configure) the muon selection tool
  m_muonSelection = new MuonSelectionTool ("MuonSelection");
  m_muonSelection->msg().setLevel( MSG::ERROR );
  ANA_CHECK(m_muonSelection->setProperty( "MaxEta", 2.5));
  ANA_CHECK(m_muonSelection->setProperty( "MuQuality", 0));
  ANA_CHECK(m_muonSelection->initialize());

  //initialize (and configure) the muon efficiency tool
//  m_effi_corr = new CP::MuonEfficiencyScaleFactorsTool( "MuonEffTool" );
//  ANA_CHECK(m_effi_corr->setProperty( "WorkingPoint", "Tight"));
//  ANA_CHECK(m_effi_corr->initialize());




  //initialize (and configure) the isolation selection tool
  m_IsoSelection = new IsolationSelectionTool ("IsoSelection");
  m_IsoSelection->msg().setLevel( MSG::ERROR );
  ANA_CHECK(m_IsoSelection->setProperty( "MuonWP","Gradient"));
  ANA_CHECK(m_IsoSelection->initialize());

  //  cout<<"D"<<endl;

  //initialize (and configure) the track selection tool
  m_trackSelection = new InDetTrackSelectionTool ("TrackSelection");
  m_trackSelection->msg().setLevel( MSG::ERROR );
  ANA_CHECK(m_trackSelection->setProperty( "CutLevel", "LoosePrimary"));
  //  ANA_CHECK(m_trackSelection->setProperty( "MuQuality", 1));
  ANA_CHECK(m_trackSelection->initialize());

  /*
  //initialize (and configure) the electron selection tool
  m_MediumLH = new AsgElectronLikelihoodTool ("MediumLH");
  // select the working point
  ANA_CHECK(m_MediumLH->setProperty("WorkingPoint", "MediumLHElectron"));
  ANA_CHECK(m_MediumLH->initialize());
  */
  
  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int
  
  // count number of events
  m_eventCounter = 0;
  m_passeventCounter = 0;
  m_numCleanEvents = 0;
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once) 

  xAOD::TEvent* event = wk()->xaodEvent();

// print every 1000 events, so we know where we are:
  if( (m_eventCounter % 1000) ==0  ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;
  
  //----------------------------
  // Event information
  //--------------------------- 
  //const xAOD::EventInfo* eventInfo = 0;
  //ANA_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));  
  static SG::AuxElement::Decorator<unsigned int> dec_rnd("RandomRunNumber");

  ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));  
  bool isMC=true;
  if (!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){

    dec_rnd(*eventInfo) = eventInfo->runNumber();
    isMC=false;
  }
  //  m_PRWTool->getRandomRunNumber( *eventInfo,true );
  //  CHECK("Execute()", m_PRWTool->getRandomRunNumber( *eventInfo ));
  CHECK("Execute()", m_PRWTool->apply(*eventInfo ));

  // m_PRWTool->apply(*eventInfo );

  //  cout<<"AFTER  corrected mu is "<<m_mu_1up<<endl;

  //  float m_mc_pu_weight = m_PRWTool->getCombinedWeight( *eventInfo );
  //  cout<< m_mc_pu_weight<<endl;
  //  ANA_CHECK( m_PRWTool->apply(*eventInfo));
  // m_PRWTool->apply(*eventInfo);
  //  cout<<"This event has a random? "<<endl;

  //  m_PRWTool->getRandomRunNumber(*eventInfo);
  //  cout<<"NOW? "<<endl;109532336
  //  if(fabs(eventInfo->eventNumber()-109745536)<1000)cout<<"BEFORE THE GRL I FOUND EVENT"<<eventInfo->eventNumber()<<" OFF BY "<<fabs(eventInfo->eventNumber()-109745536)<<endl;
  //  if(eventInfo->lumiBlock()==209)cout<<"BEFORE THE GRL I FOUND EVENT "<<eventInfo->eventNumber()<<" OFF BY "<<fabs(eventInfo->eventNumber()-129151000)<<endl;
  if(!m_grl->passRunLB(*eventInfo) && !m_grl2->passRunLB(*eventInfo) && !eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION )){
    return EL::StatusCode::SUCCESS; // go to next event
  }

  if(   (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )  )
    {
      return EL::StatusCode::SUCCESS; // go to the next event
    } // end if event flags check
  m_numCleanEvents++;
  
  EventNumber = eventInfo->eventNumber();
  RunNumber = eventInfo->runNumber();
  lbn = eventInfo->lumiBlock();
  bcid = eventInfo->bcid();
  averageIntPerXing = eventInfo->averageInteractionsPerCrossing();
  actualIntPerXing =  eventInfo->actualInteractionsPerCrossing();
  mufromtool = m_PRWTool->getCorrectedMu(*eventInfo, true);
  utps64=-88;
  auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_mu.*");
  std::map<std::string,int> triggerCounts;
  for(auto &trig : chainGroup->getListOfTriggers()) {
    auto cg = m_trigDecisionTool->getChainGroup(trig);
    std::string thisTrig = trig;
    //    std::string wanted ("HLT_mu6_mu4_bUpsimumu'");
    if(thisTrig=="HLT_mu6_mu4_bUpsimumu") 
      {
	if(cg->isPassed())utps64 = cg->getPrescale();
	else utps64 =-98;
      }
    //    if ( cg->getPrescale()==1) Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
  } 

  utps44=-88;
  utps66=-88;
  auto chainGroup2 = m_trigDecisionTool->getChainGroup("HLT_2mu.*");
  std::map<std::string,int> triggerCounts2;
  for(auto &trig2 : chainGroup2->getListOfTriggers()) {
    auto cg = m_trigDecisionTool->getChainGroup(trig2);
    std::string thisTrig = trig2;
    if(thisTrig=="HLT_2mu4_bUpsimumu") 
     {
	if(cg->isPassed())utps44 = cg->getPrescale();
	else utps44 =-98;
      }
    if(thisTrig=="HLT_2mu6_bUpsimumu")
      {
	if(cg->isPassed())utps66 = cg->getPrescale();
	else utps66 =-98;
      }
      //       Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
    //    if ( cg->getPrescale()==1) Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
  } 



  //------------
  //Vertex
  //------------
  //  cout<<"Finished the trig stuff at least once"<<endl;

  const xAOD::VertexContainer* pvxs = 0;
  //ANA_CHECK(event->retrieve( pvxs, "PrimaryVertices" ));
  ANA_CHECK(event->retrieve( pvxs, "PrimaryVertices" ));
  //loop over vertices
  xAOD::VertexContainer::const_iterator pvx_itr = pvxs->begin();
  xAOD::VertexContainer::const_iterator pvx_end = pvxs->end();
  //  xAOD::VertexContainer::const_iterator pvx_primary;
  primary_vx_z=-9999;
  truth_primary_vx_z=-9999;
  //  double primary_vx_x=-9999;
  //  double primary_vx_y=-9999;
  Nvtxs=0;
  float pt2;
  int zposbin=-1;
  for(const xAOD::Vertex* vx : *pvxs  ) //loop once only to count nvtx and get the primary vertex
    {
      if((vx)->vertexType() !=0) Nvtxs++;
      else continue;
      if( (vx)->vertexType() == xAOD::VxType::PriVtx) //xAOD::VxType::PriVtx is just 1 but whatever
	{
	  primary_vx_z=(vx)->z();
	  //if(fabs(primary_vx_z)>150) return EL::StatusCode::SUCCESS;//150mm cut
	  zposbin = getzposbin(primary_vx_z);
	}
      else
	{
	  vxs->push_back( vx->z());
	}
    }
  //  cout<<"first other count = "<<vxs->size()<<" vs "<<Nvtxs<<endl;
  if(primary_vx_z==-9999) {cout<<"No primary vx reconstructed!"<<endl;return EL::StatusCode::SUCCESS;} // go to the next event if there's no primary vertex
  //  cout<<"second other count = "<<vxs->size()<<" vs "<<Nvtxs<<endl;
  //  cout<<"Number of vertices is "<<Nvtxs<<endl;


  //  cout<<"Finished the vertex stuff at least once"<<endl;
  //Look at the truth level stuff
  Upsm = -999;
  Upspt =-999;
  Upsy = -999;
  Upsphi = -999;
  UpsID = -999;
  UpsParentID = -999;

  truemupt1=-999;
  truemupt2=-999;
  truemueta1=-999;
  truemueta2=-999;
  truemuphi1=-999;
  truemuphi2=-999;
  truemuch1=-999;
  truemuch2=-999;


  //load up the tracks container even though not used until later: *inside* jets loop and then dedicated loop after jets
  //------------
  //TRACKS
  //------------
  const xAOD::TrackParticleContainer* tracks = 0;
  //ANA_CHECK(event->retrieve( tracks, "InDetTrackParticles" ));
  ANA_CHECK(event->retrieve( tracks, "InDetTrackParticles" ));
  xAOD::TrackParticleContainer::const_iterator track_itr;// = tracks->begin();
  xAOD::TrackParticleContainer::const_iterator track_end;// = tracks->end();
  //  cout<<"opened the tracks"<<endl;

  int MC_state=0;
  int Ups_ID[3]={553,100553,200553};
  if(isMC)
    {

      const xAOD::TruthVertexContainer* true_vxs = 0;
      ANA_CHECK(event->retrieve(true_vxs, "TruthVertices" ));
     
      //      double first_vx=-999;
      //      double second_vx=-999;
      xAOD::TruthVertexContainer::const_iterator tvItr = true_vxs->begin();
      xAOD::TruthVertexContainer::const_iterator tvItrE = true_vxs->end();
      const xAOD::TruthVertex* vertex = (*tvItr);
      truth_primary_vx_z = vertex->z();
//      truth_Npup_vxs=-1;
//      for(; tvItr!=tvItrE; ++tvItr){
//	truth_Npup_vxs++;
//      }

      const xAOD::TruthParticleContainer* trues = 0;
      ANA_CHECK(event->retrieve(trues, "TruthParticles" ));

      xAOD::TruthParticleContainer::const_iterator tpItr = trues->begin();
      xAOD::TruthParticleContainer::const_iterator tpItrE = trues->end();

      xAOD::TruthParticleContainer::const_iterator tpJItr = trues->begin();
      xAOD::TruthParticleContainer::const_iterator tpJItrE = trues->end();

      //float UpsPrimaryVtx=-9999;

      for(; tpItr!=tpItrE; ++tpItr){
        const xAOD::TruthParticle* particle = (*tpItr);
	//first get the upsilon
	if(particle->pdgId()!=Ups_ID[MC_state])continue;
//	if(particle->pt()<390) continue;
	if(fabs(particle->rapidity())>2.5) continue;
//	if(particle->pdgId()==22)continue;

//	double mu1vert=-9999;
//	double mu2vert=-9999;

//to make sure this is the real upsilon that goes to dimuons check the children now
        if (particle->nChildren()>0) {
          bool first=true;
          for (unsigned int i=0; i<particle->nChildren(); i++) {
            const xAOD::TruthParticle* Upschild = particle->child( i );

            if(Upschild->status()!=1) continue;//skip documentary particles
            if(!Upschild->isMuon()) continue;//don't care about non-muon children
            if(first)
              {
                truemupt1 = Upschild->pt()/1000.;
                truemuphi1 = Upschild->phi();
                truemueta1 = Upschild->eta();
                truemuch1 = Upschild->charge();
                first=false;
		//		ElementLink< xAOD::TruthVertexContainer > tempverLink = Upschild->auxdata<ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
		//		if(tempverLink.isValid())mu1vert=(*tempverLink)->z();
              }
            else
              {
                truemupt2 = Upschild->pt()/1000.;
                truemuphi2 = Upschild->phi();
                truemueta2 = Upschild->eta();
                truemuch2 = Upschild->charge();
		//		ElementLink< xAOD::TruthVertexContainer > tempverLink = Upschild->auxdata<ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
		//		if(tempverLink.isValid())mu2vert=(*tempverLink)->z();
              }
          }//end loop over children
        }//end if for children

	if(truemupt1<-900 || truemupt2<-900) continue;

	//	ElementLink< xAOD::TruthVertexContainer > truthverLink = particle->auxdata<ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
	//	if(truthverLink.isValid()){truth_primary_vx_z=(*truthverLink)->z();}//cout<<"First = "<<first_vx<<", second = "<<second_vx<<", from upsilon = "<<truth_primary_vx_z<<endl;}
//	else //try to find the first ancestor and get the primary vertex from there 
//	  {
//	    if(particle->nParents()>0)
//	      {
//		const xAOD::TruthParticle *vparent = particle->parent(particle->nParents()-1);
//		ElementLink< xAOD::TruthVertexContainer > vtruthverLink = vparent->auxdata<ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
//		if(vtruthverLink.isValid()){truth_primary_vx_z=(*vtruthverLink)->z();cout<<"First = "<<first_vx<<", second = "<<second_vx<<", from upsilon parent = "<<truth_primary_vx_z<<endl;}
//		
//	      }
//	  }
//	if(truth_primary_vx_z<-900) {truth_primary_vx_z=mu1vert;cout<<"First = "<<first_vx<<", second = "<<second_vx<<", from upsilon muon = "<<truth_primary_vx_z<<endl;}
//	if(truth_primary_vx_z<-900) truth_primary_vx_z=mu2vert;
//
//	if(truth_primary_vx_z<-900) {cout<<"No vertex from truth upsilon!"<<endl;return EL::StatusCode::SUCCESS; }// go to the next event
//
	if(fabs(truth_primary_vx_z)>500) {cout<<"Out of bounds vertex from truth upsilon!"<<endl;return EL::StatusCode::SUCCESS; }// go to the next event

        Upsm = particle->m()/1000.;
        Upspt = particle->pt()/1000.;
        Upsy = particle->rapidity();
        Upsphi = particle->phi();//Upsphi = particle->nChildren();
        UpsID = particle->pdgId();//UpsID = particle->status();


	
	//look for Upsilon parent and siblings/cousins
	UpsParentID = -888.8;
	if(particle->nParents()>0)
	  {
	    for(unsigned int i=0; i<particle->nParents(); i++) {
	    const xAOD::TruthParticle *parent = particle->parent(i);
	    //	    if(parent->pdgId()!=100553&&parent->pdgId()!=200553)continue;
	    bool right_parent=0;
	    for(int usi=MC_state+1;usi<3;usi++) {if (parent->pdgId()==Ups_ID[usi]) right_parent=true;}
	    if(!right_parent)continue;

	    UpsParentID = parent->pdgId();
	    }
	    if(UpsParentID>-888.7||UpsParentID<-888.9){//if there's an excited upsilon parent for the 1S look for siblings
	      for(; tpJItr!=tpJItrE; ++tpJItr){
		const xAOD::TruthParticle* maybesibling = (*tpJItr);
		if(maybesibling->status()!=1 || maybesibling->isMuon() || maybesibling->pdgId()==22 ) continue;//if it's NOT final state or IS a muon/photon skip it
		if(maybesibling->nParents()>0)
		  {
		    bool is_real_sibling=0;
		    for(unsigned int i=0; i<maybesibling->nParents(); i++) {
		      const xAOD::TruthParticle *parent = maybesibling->parent(i);
		      //if(parent->pdgId()!=100553&&parent->pdgId()!=200553)continue;
		      bool aright_parent=0;
		      for(int usi=MC_state+1;usi<3;usi++) {if (parent->pdgId()==Ups_ID[usi]) aright_parent=true;}
		      if(!aright_parent)continue;
		      is_real_sibling=true;
		    }
		    if( is_real_sibling)
		      {
			truesib_ID->push_back(maybesibling->pdgId());
			truesib_pt->push_back(maybesibling->pt()/1000.);
			truesib_phi->push_back(maybesibling->phi());
			truesib_eta->push_back(maybesibling->eta());

			int sib_reco=0;
			float sibreco_z0=-9999;

			track_itr = tracks->begin();
			track_end = tracks->end();
			for( ; track_itr != track_end; ++track_itr )
			  {
			    ElementLink< xAOD::TruthParticleContainer > truthLink = (*track_itr)->auxdata<ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");
			    if(truthLink.isValid())
			      {
				if((*truthLink)->pt() == maybesibling->pt() )//found the reco track
				  {

				    float d0_significance = fabs((*track_itr)->d0()) / sqrt((*track_itr)->definingParametersCovMatrix()(0,0));
				    sib_reco=(fabs((*track_itr)->eta()) <2.5) +2*(m_trackSelection->accept(**track_itr)) +4*(fabs((*track_itr)->d0() ) <1.5 && d0_significance<3);
				    sibreco_z0 = (*track_itr)->z0() + (*track_itr)->vz() - primary_vx_z;
				    track_itr=track_end-1;
				  }//close if reco matches truuth
			      }//close if reco has a truth link
			  }//close loop over reco tracks

			truesib_recoq->push_back(sib_reco);
			truesib_recoz0->push_back(sibreco_z0);
		      }//close if real sibling
		  }//close if putative sibling has parents
	      }//close jitr loop over truth

	    }//close if found an upsilon parent
	  }//close if particle has parent
      }//close loop over truth just for upsilon

      //      if(truth_primary_vx_z==-9999) return EL::StatusCode::SUCCESS; // go to the next event

      //      cout<<"Finished the upsilon stuff at least once"<<endl;      
      //now loop again for all tracks
      tpItr = trues->begin();
      tpItrE = trues->end();
      
      for(; tpItr!=tpItrE; ++tpItr){
	const xAOD::TruthParticle* particle = (*tpItr);
	//now that I'm done looking for the upsilon let me get truth tracks
	if(particle->pt()<390) continue;
	//	if(fabs(particle->eta())>2.5) continue;
	if(particle->pdgId()==22)continue;
	if(particle->status()!=1 ) continue;
	if(particle->barcode()>=200000 ) continue;
	bool is_mu_from_ups =0;
	const xAOD::TruthParticle *tparent = particle->parent(0);
	if(tparent)
	  {
	    if(tparent->pdgId()==Ups_ID[MC_state] && particle->isMuon()) is_mu_from_ups=true;
	  }
	if(is_mu_from_ups==false)
	  {

	    double negflip =-1;//default to say it's pileup
	    ElementLink< xAOD::TruthVertexContainer > truthverLink = particle->auxdata<ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
	    if(truthverLink.isValid())
	      {
		//if(fabs(truth_primary_vx_z-(*truthverLink)->z())<1.5  ) negflip=1;
		if(truth_primary_vx_z==(*truthverLink)->z()) negflip=1;//if it's from the PV
		//	else if ((*truthverLink)->vertexType()==2) negflip=0;//if it's a secondary
//		else if(particle->nParents()>0)
//		  {
//		    const xAOD::TruthParticle *vparent = particle->parent(particle->nParents()-1);
//		    ElementLink< xAOD::TruthVertexContainer > vtruthverLink = vparent->auxdata<ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
//		    if(vtruthverLink.isValid())
//		      {
//			if(fabs(truth_primary_vx_z-(*vtruthverLink)->z())<1.5  ) negflip=1;
//			//if(truth_primary_vx_z==(*vtruthverLink)->z())negflip=1;
//		      }
//		  }
		//	else{cout<<"PV is at "<<truth_primary_vx_z<<" this particle's vertex is at "<<(*truthverLink)->z()<<endl;}
		//		else cout<<"from Upsilon it's "<<truth_primary_vx_z<<" but this particle has "<<(*truthverLink)->z()<<endl;
		truetrk_vxz->push_back((*truthverLink)->z());
		truetrk_vxy->push_back((*truthverLink)->y());
		truetrk_vxx->push_back((*truthverLink)->x());
	      }
	    else {negflip=0;cout<<"No vertex link!"<<endl;truetrk_vxz->push_back(-99999);truetrk_vxx->push_back(-99999);truetrk_vxy->push_back(-99999);}
	    truetrk_pt->push_back(negflip*particle->pt()/1000.);
	    truetrk_charge->push_back(particle->charge());
	    truetrk_eta->push_back(particle->eta());
	    truetrk_phi->push_back(particle->phi());

	    
	    int trk_reco=0;
	    float trk_z0=-9999;
	    
	    track_itr = tracks->begin();
	    track_end = tracks->end();
	    for( ; track_itr != track_end; ++track_itr )
	      {
		ElementLink< xAOD::TruthParticleContainer > truthLink = (*track_itr)->auxdata<ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");
		if(truthLink.isValid())
		  {
		    if((*truthLink)->pt() == particle->pt() )//found the reco track
		      {
			float d0_significance = fabs((*track_itr)->d0()) / sqrt((*track_itr)->definingParametersCovMatrix()(0,0));
			trk_reco=(fabs((*track_itr)->eta()) <2.5) +2*(m_trackSelection->accept(**track_itr)) +4*(fabs((*track_itr)->d0() ) <1.5 && d0_significance<3);
			trk_z0 = (*track_itr)->z0() + (*track_itr)->vz() - primary_vx_z;
			track_itr=track_end-1;
		      }//close if reco matches truuth
		  }//close if reco has a truth link
	      }//close loop over reco tracks
	    //	    if(fabs(trk_z0)<1&&negflip==-1)cout<<"from Upsilon it's "<<truth_primary_vx_z<<" but this particle has "<<(*truthverLink)->z()<<endl;
	    truetrk_recoq->push_back(trk_reco);
	    truetrk_recoz0->push_back(trk_z0);
	  }//close chose a good truth particle - not muons from upsilons
      }//end (2nd) loop over truth particles

    }//close if(isMC)

  //  cout<<"finished the ifMC loop at least once"<<endl;

  /*
  //------------
  //ELECTRONS
  //------------
  int ecount=0;
  const xAOD::ElectronContainer* electrons = 0;
  ANA_CHECK(event->retrieve( electrons, "Electrons" ));

 // create a shallow copy of the electrons container
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons );

 // loop over the electrons in the shallow copy of the container
  //  xAOD::ElectronContainer::iterator elec_itr = (electrons_shallowCopy.first)->begin();
  //xAOD::ElectronContainer::iterator elec_end = (electrons_shallowCopy.first)->end();
  xAOD::ElectronContainer::const_iterator elec_itr = electrons->begin();
  xAOD::ElectronContainer::const_iterator elec_end = electrons->end();
  const xAOD::TrackParticle* tp =0;

  for( ; elec_itr != elec_end; ++elec_itr )
    {
      //      if( ! (*elec_itr)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) ) continue;
      if (  (*elec_itr)->pt()*0.001 < 20 )continue;
      if(! m_MediumLH->accept( (*elec_itr) ) ) continue;
      xAOD::ElectronContainer::const_iterator elec_jitr = (elec_itr)+1;
      for( ; elec_jitr != elec_end; ++elec_jitr ) 
	{ 
	  //	  if(m_egammaCalibrationAndSmearingTool->applyCorrection(**elec_jitr) == CP::CorrectionCode::Error){ 
	  //	    Error("execute()", "egammaCalibrationAndSmearingTool returns Error CorrectionCode");
	  //	  }
	  if (  (*elec_jitr)->pt()*0.001 < 20 )continue;
	  if(! m_MediumLH->accept( (*elec_jitr) ) ) continue;
	  TLorentzVector t1,t2,tpair;
	  t1.SetPtEtaPhiM((*elec_itr)->pt() * 0.001,(*elec_itr)->eta(),(*elec_itr)->phi(),0.511*0.001 );
	  t2.SetPtEtaPhiM((*elec_jitr)->pt() * 0.001,(*elec_jitr)->eta(),(*elec_jitr)->phi(),0.511*0.001 );
	  tpair = t1+t2;
	  if(tpair.M()>60&&tpair.M()<120) ecount++;
	    }
    }
  */




  //  cout<<"count is "<<count<<endl;  
//  if(pairpt<40){
//    return EL::StatusCode::SUCCESS; // go to the next event
//  }
//  cout<<"done with muons"<<endl;

  // get jet container of interest
  const xAOD::JetContainer* jets = 0;
  ANA_CHECK(event->retrieve( jets, "AntiKt4EMTopoJets" ));
  //  const xAOD::JetContainer* jets10 = 0;
  //  ANA_CHECK(event->retrieve( jets10, "AntiKt10LCTopoJets" ));
  //  cout<<"opened jet containers"<<endl;
  //  int numGoodJets = 0;   
  // loop over the jets in the container
  float hi_jpt=10;
  float hi_jeta=-9999;
  float hi_jphi=-9999;

  xAOD::TStore store;
  xAOD::JetContainer::const_iterator jet_itr = jets->begin();
  xAOD::JetContainer::const_iterator jet_end = jets->end();
  for( ; jet_itr != jet_end; ++jet_itr ) {
    xAOD::Jet * this_calibjet = 0;
    if( !m_jetCleaning->accept( **jet_itr )) continue; //only keep good clean jets
   
    m_jetCalibration->calibratedCopy(**jet_itr,this_calibjet); //make a calibrated copy
    if(this_calibjet->pt() * 0.001 <18) continue; //testf was run with 15

    //    if(getdR(recoeta1,recophi1,this_calibjet->eta(),this_calibjet->phi() )<0.4) continue;
    //    if(getdR(recoeta2,recophi2,this_calibjet->eta(),this_calibjet->phi() )<0.4) continue;

    if(this_calibjet->pt() * 0.001 > hi_jpt) 
      {
	hi_jpt=this_calibjet->pt() * 0.001;//the highest jet pT I've read so far
	hi_jeta=this_calibjet->eta();
	hi_jphi=this_calibjet->phi();
      }

    jet4_pt->push_back(this_calibjet->pt() * 0.001 );
    jet4_uncalibpt->push_back((*jet_itr)->pt() * 0.001 );
    jet4_eta->push_back(this_calibjet->eta() );
    jet4_phi->push_back(this_calibjet->phi() );
    jet4_jvf->push_back(hjvtagup->updateJvt(*this_calibjet)); 

  } // end for loop over jets

  //Fill the vertex/track histos
  closest_pup=500;
  for(const xAOD::Vertex* vx : *pvxs  )  //loop again to fill histos with knowledge of primary vtx and Nvtx from previous loop
    {
      if( (vx)->vertexType() == xAOD::VxType::PriVtx) //xAOD::VxType::PriVtx is just 1 but whatever
	{
	  pt2=sqrt(( vx)->auxdata< float >( "sumPt2" ));
	  //	  primary_pt2->Fill(pt2,Nvtxs);
	  //	  if(jet4_pt->size()==1)  primary_pt2_onejet->Fill(pt2,Nvtxs);
	}
      else if( (vx)->vertexType()!=0 )//a pileup vertex (type==0 is the placeholder one)
	{
	  //	  vtxspacing->Fill(primary_vx_z-(vx)->z(),Nvtxs);
	  if(fabs(primary_vx_z-(vx)->z())<closest_pup) closest_pup=fabs(primary_vx_z-(vx)->z());
	  pt2=sqrt(( vx)->auxdata< float >( "sumPt2" ));
	  //	  secondary_pt2->Fill(pt2,Nvtxs);
	  //	  if(jet4_pt->size()==1)  secondary_pt2_onejet->Fill(pt2,Nvtxs);
	}
    }

  //  cout<<"finished the jets at least once"<<endl;

  //------------
  // MUONS
  //------------

  // get muon container of interest
  const xAOD::MuonContainer* muons = 0;
  ANA_CHECK(event->retrieve( muons, "Muons" ));
  


 // create a shallow copy of the muons container
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
  
  // iterate over our shallow copy
  xAOD::MuonContainer::iterator muon_itr = (muons_shallowCopy.first)->begin();
  xAOD::MuonContainer::iterator muon_end = (muons_shallowCopy.first)->end();
  
  int count=0;
  //  int Ztrackstosubtract=0;

  // xAOD::MuonContainer::const_iterator muon_itr = muons->begin();
  //  xAOD::MuonContainer::const_iterator muon_end = muons->end();
  bool trigger_matched=false;
  const xAOD::TrackParticle* m1track =0;
  const xAOD::TrackParticle* m2track =0;
  double muon_etas[2]={-100,-100};
  double muon_pts[2]={-10000,-10000};
  double muon_phis[2]={-100,-100};
  for( ; muon_itr != muon_end; ++muon_itr )
    {
      if(m_muonCalibrationAndSmearingTool->applyCorrection(**muon_itr) == CP::CorrectionCode::Error){ // apply correction and check return code
	// Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
	// If OutOfValidityRange is returned no modification is made and the original muon values are taken.
	Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
      }
      if ( (*muon_itr)->pt() * 0.001 <4 ) continue;

      //      if ( (*muon_itr)->pt() * 0.001 <15 ) continue;
      if(!m_muonSelection->accept(**muon_itr)) continue;
      //      if(!m_IsoSelection->accept(**muon_itr)) continue;

      //make sure the muon is from the primary vertex
      //const xAOD::TrackParticle* m1track =  (*muon_itr)->primaryTrackParticle();
      bool notprim=false;
      m1track =  (*muon_itr)->primaryTrackParticle();
      //      if (!m1track) continue;//this is the real way (1/2)
      //      if( fabs(m1track->z0() +m1track->vz()- primary_vx_z)*sin(m1track->theta()) >1.) continue;//this is the real way (2/2)
      if (m1track) 
	{
	  if( fabs(m1track->z0() +m1track->vz()- primary_vx_z)*sin(m1track->theta()) >1.)notprim=true;
	}
      else notprim=true;

      float effholder1 =-99;
      if(m_effi_corr->getDataEfficiency(**muon_itr,effholder1) == CP::CorrectionCode::Error){ // get eff and check return code
	Error("execute()", "MuonEfficiencyScaleFactorsTool returns Error CorrectionCode");
      }


      xAOD::MuonContainer::iterator muon_jitr=(muon_itr)+1;
      for( ; muon_jitr != muon_end; ++muon_jitr ) 
	{ 
	  if(m_muonCalibrationAndSmearingTool->applyCorrection(**muon_jitr) == CP::CorrectionCode::Error){ // apply correction and check return code
	    Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
	  }
	  //  if ( (*muon_jitr)->pt() * 0.001 <20 ) continue;	  

	  if ( (*muon_jitr)->pt() * 0.001 <4 ) continue;	  
	  if(!m_muonSelection->accept(**muon_jitr)) continue;
	  //	  if ( (*muon_jitr)->quality() <1 ) continue;	  //must be at least a medium muon

	  //make sure the second muon is from the primary vertex
	  //const xAOD::TrackParticle* m2track =  (*muon_jitr)->primaryTrackParticle();
	  m2track =  (*muon_jitr)->primaryTrackParticle();
	  //      if (!m2track) continue;//this is the real way (1/2)
	  //      if( fabs(m2track->z0() +m2track->vz()- primary_vx_z)*sin(m2track->theta()) >1.) continue;//this is the real way (2/2)
	  if (m2track)
	    {
	      if( fabs(m2track->z0() +m2track->vz()- primary_vx_z)*sin(m2track->theta()) >1.)notprim=true;
	    }
	  else notprim=true;


	  float effholder2 =-99;
	  if(m_effi_corr->getDataEfficiency(**muon_jitr,effholder2) == CP::CorrectionCode::Error){ // get eff and check return code
	    Error("execute()", "MuonEfficiencyScaleFactorsTool returns Error CorrectionCode");
	  }
	  vector<float> * temp1= new vector<float>;
	  float errholder1=-99;
	  vector<float> * temp2= new vector<float>;
	  float errholder2=-99;
	  ANA_CHECK(m_effi_corr->applySystematicVariation(statup));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);	  
	  ANA_CHECK(m_effi_corr->applySystematicVariation(statdown));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);	  
	  ANA_CHECK(m_effi_corr->applySystematicVariation(sysup));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);	  
	  ANA_CHECK(m_effi_corr->applySystematicVariation(sysdown));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);

	  ANA_CHECK(m_effi_corr->applySystematicVariation(loptstatup));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);	  
	  ANA_CHECK(m_effi_corr->applySystematicVariation(loptstatdown));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);	  
	  ANA_CHECK(m_effi_corr->applySystematicVariation(loptsysup));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);	  
	  ANA_CHECK(m_effi_corr->applySystematicVariation(loptsysdown));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_itr,errholder1));
	  ANA_CHECK(m_effi_corr->getDataEfficiency(**muon_jitr,errholder2));
	  temp1->push_back(errholder1);	  
	  temp2->push_back(errholder2);	  


	  TLorentzVector r1,r2,rpair;
	  r1.SetPtEtaPhiM((*muon_itr)->pt() * 0.001,(*muon_itr)->eta(),(*muon_itr)->phi(),mumass );
	  r2.SetPtEtaPhiM((*muon_jitr)->pt() * 0.001,(*muon_jitr)->eta(),(*muon_jitr)->phi(),mumass );
	  rpair = r1+r2;
	  muon_etas[0]=(*muon_itr)->eta();
	  muon_etas[1]=(*muon_jitr)->eta();
	  muon_pts[0]=(*muon_itr)->pt()/1000.;
	  muon_pts[1]=(*muon_jitr)->pt()/1000.;
	  muon_phis[0]=(*muon_itr)->phi();
	  muon_phis[1]=(*muon_jitr)->phi();
	  if((rpair.M()>7 && rpair.M()<16)||(rpair.M()<120&&rpair.M()>60))
	    {
	      count++;      
	      recoerr1->push_back(*temp1);
	      recoerr2->push_back(*temp2);
	      temp1->clear();
	      temp2->clear();
	      recoeff1 ->push_back( effholder1 );
	      recopt1 ->push_back( (*muon_itr)->pt() * 0.001 );
	      recoeta1 ->push_back( (*muon_itr)->eta() );
	      recophi1 ->push_back( (*muon_itr)->phi());
	      recoch1 ->push_back( (*muon_itr)->charge());
	      recod01 ->push_back( m1track->d0());
	      recod0s1 ->push_back( fabs(m1track->d0()) / sqrt(m1track->definingParametersCovMatrix()(0,0)));
	      recotosub1->push_back(m_trackSelection->accept(m1track) && fabs(m1track->d0())<=1.5 && (fabs(m1track->d0()) / sqrt(m1track->definingParametersCovMatrix()(0,0)))<=3 );
	      recoiso1->push_back(m_IsoSelection->accept(**muon_itr));	  
	      //trig bit is 1*4,4 + 2*6,4 + 4*6,6 + 8*10,10||14,14 + 16*18s||20s +32*24s||26s
	      int trigbit = ( m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_11invm60_noos")  ||   m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_11invm60_noos_novtx") ||  m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_7invm9_noos") ||   m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_7invm9_noos_novtx"))  
		+2*(m_match_Tool->match(*muon_itr,"HLT_mu6_mu4_bBmumu") || m_match_Tool->match(*muon_itr,"HLT_mu6_mu4_bUpsimumu") || m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_11invm60_noos_L1_MU6_2MU4") || m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_11invm60_noos_novtx_L1_MU6_2MU4")  || m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_7invm9_noos_L1_MU6_2MU4") ||  m_match_Tool->match(*muon_itr,"HLT_mu4_iloose_mu4_7invm9_noos_novtx_L1_MU6_2MU4") )
		+4*(m_match_Tool->match(*muon_itr,"HLT_2mu6_10invm30_pt2_z10") || m_match_Tool->match(*muon_itr,"HLT_mu6_iloose_mu6_11invm24_noos") || m_match_Tool->match(*muon_itr,"HLT_mu6_iloose_mu6_11invm24_noos_novtx") || m_match_Tool->match(*muon_itr,"HLT_mu6_iloose_mu6_24invm60_noos") || m_match_Tool->match(*muon_itr,"HLT_mu6_iloose_mu6_24invm60_noos_novtx") || m_match_Tool->match(*muon_itr,"HLT_2mu6_bUpsimumu") || m_match_Tool->match(*muon_itr,"HLT_2mu6_bBmumu") )
		+8*( m_match_Tool->match(*muon_itr,"HLT_2mu10") ||  m_match_Tool->match(*muon_itr,"HLT_2mu14"))
		+16*( m_match_Tool->match(*muon_itr,"HLT_mu18_mu8noL1") ||  m_match_Tool->match(*muon_itr,"HLT_mu20_iloose_L1MU15")) 
		+32*(m_match_Tool->match(*muon_itr,"HLT_mu24_imedium") || m_match_Tool->match(*muon_itr,"HLT_mu26_ivarmedium") || m_match_Tool->match(*muon_itr,"HLT_mu24_iloose") || m_match_Tool->match(*muon_itr,"HLT_mu24_iloose_L1MU15") ) ;
	      recotrig1->push_back(trigbit);

	      recoutrig1->push_back( m_match_Tool->match(*muon_itr,"HLT_2mu4_bUpsimumu") +2* m_match_Tool->match(*muon_itr,"HLT_mu6_mu4_bUpsimumu") +4* m_match_Tool->match(*muon_itr,"HLT_2mu6_bUpsimumu") );


	      recoeff2 ->push_back( effholder2 );
	      recopt2 ->push_back( (*muon_jitr)->pt() * 0.001 );
	      recoeta2 ->push_back( (*muon_jitr)->eta() );
	      recophi2 ->push_back( (*muon_jitr)->phi());
	      recoch2 ->push_back( (*muon_jitr)->charge());
	      recod02 ->push_back( m2track->d0());
	      recod0s2 ->push_back( fabs(m2track->d0()) / sqrt(m2track->definingParametersCovMatrix()(0,0)));
	      recotosub2->push_back(m_trackSelection->accept(m2track) && fabs(m2track->d0())<=1.5 && (fabs(m2track->d0()) / sqrt(m2track->definingParametersCovMatrix()(0,0)))<=3 );
	      recoiso2->push_back(m_IsoSelection->accept(**muon_jitr));	  
	      //trig bit is 1*4,4 + 2*6,4 + 4*6,6 + 8*10,10||14,14 + 16*18s||20s +32*24s||26s
	      trigbit = ( m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_11invm60_noos")  ||   m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_11invm60_noos_novtx") ||  m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_7invm9_noos") ||   m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_7invm9_noos_novtx"))  
		+2*(m_match_Tool->match(*muon_jitr,"HLT_mu6_mu4_bBmumu") || m_match_Tool->match(*muon_jitr,"HLT_mu6_mu4_bUpsimumu") || m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_11invm60_noos_L1_MU6_2MU4") || m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_11invm60_noos_novtx_L1_MU6_2MU4")  || m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_7invm9_noos_L1_MU6_2MU4") ||  m_match_Tool->match(*muon_jitr,"HLT_mu4_iloose_mu4_7invm9_noos_novtx_L1_MU6_2MU4") )
		+4*(m_match_Tool->match(*muon_jitr,"HLT_2mu6_10invm30_pt2_z10") || m_match_Tool->match(*muon_jitr,"HLT_mu6_iloose_mu6_11invm24_noos") || m_match_Tool->match(*muon_jitr,"HLT_mu6_iloose_mu6_11invm24_noos_novtx") || m_match_Tool->match(*muon_jitr,"HLT_mu6_iloose_mu6_24invm60_noos") || m_match_Tool->match(*muon_jitr,"HLT_mu6_iloose_mu6_24invm60_noos_novtx") || m_match_Tool->match(*muon_jitr,"HLT_2mu6_bUpsimumu") || m_match_Tool->match(*muon_jitr,"HLT_2mu6_bBmumu") )
		+8*( m_match_Tool->match(*muon_jitr,"HLT_2mu10") ||  m_match_Tool->match(*muon_jitr,"HLT_2mu14"))
		+16*( m_match_Tool->match(*muon_jitr,"HLT_mu18_mu8noL1") ||  m_match_Tool->match(*muon_jitr,"HLT_mu20_iloose_L1MU15")) 
		+32*(m_match_Tool->match(*muon_jitr,"HLT_mu24_imedium") || m_match_Tool->match(*muon_jitr,"HLT_mu26_ivarmedium") || m_match_Tool->match(*muon_jitr,"HLT_mu24_iloose") || m_match_Tool->match(*muon_jitr,"HLT_mu24_iloose_L1MU15") ) ;
	      recotrig2->push_back(trigbit);
	      recoutrig2->push_back( m_match_Tool->match(*muon_jitr,"HLT_2mu4_bUpsimumu") +2* m_match_Tool->match(*muon_jitr,"HLT_mu6_mu4_bUpsimumu") +4* m_match_Tool->match(*muon_jitr,"HLT_2mu6_bUpsimumu") );

	      pairpt->push_back(rpair.Pt());
	      pairy->push_back(rpair.Rapidity());
	      paireta->push_back(rpair.Eta());
	      if(notprim) pairm->push_back(-rpair.M());
	      else pairm->push_back(rpair.M());
	      pairphi->push_back(rpair.Phi());

	      //	      pairs->Fill();
	      //	      if(m_trackSelection->accept(*m1track)) Ztrackstosubtract++;
	      //	      if(m_trackSelection->accept(m2track)) Ztrackstosubtract++;
	    }
	}
      //    Info("execute()", "  original muon pt = %.2f GeV", ((*muon_itr)->pt() * 0.001)); // just to print out something
    } // end for loop over muons
  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;
  if(count)m_passeventCounter++;
  
// if(count!=1){//require exactly one muon pair consistent with a Z kinematically (may be same sign pair) 
//   vxs->clear();
//   return EL::StatusCode::SUCCESS; // go to the next event
// }
//  cout<<"finished the muons at least once"<<endl;


  //------------
  //TRACKS
  //------------
  //loop over tracks
  track_itr = tracks->begin();
  track_end = tracks->end();
  Noftracks=0;
  Nofbkgs=0;

//  //flipped detector jet spot
//  float flip_eta=-99;
//  if(hi_jpt>10){
//    flip_eta = - hi_jeta;
//    if(flip_eta<0 &&flip_eta>-0.4) flip_eta =-0.4;
//    if(flip_eta>0 &&flip_eta<0.4) flip_eta =0.4;
//  }//  cout<<"flip eta is "<<flip_eta<<endl;
//  cout<<"the muons are "<<m1track->pt()<<", "<<m1track->eta() <<"and "<<m2track->pt()<<", "<<m2track->eta() <<endl;
//  float  everything=0;
  for( ; track_itr != track_end; ++track_itr )
    {
      //      everything++;
      if ( fabs((*track_itr)->eta()) >2.5 ) continue;	  //not looking at tracks  outside 2.5
      if(!m_trackSelection->accept(**track_itr)) continue;

      if(getdR((*track_itr)->eta(),(*track_itr)->phi(),muon_etas[0],muon_phis[0])<0.01 && ((*track_itr)->pt()/1000.)/muon_pts[0]>0.8 && ((*track_itr)->pt()/1000.)/muon_pts[0]<1.2 )continue;
      if(getdR((*track_itr)->eta(),(*track_itr)->phi(),muon_etas[1],muon_phis[1])<0.01 && ((*track_itr)->pt()/1000.)/muon_pts[1]>0.8 && ((*track_itr)->pt()/1000.)/muon_pts[1]<1.2)continue;

      if ( fabs((*track_itr)->d0() ) >1.5 ) continue;	  
      float d0_significance = fabs((*track_itr)->d0()) / sqrt((*track_itr)->definingParametersCovMatrix()(0,0));
      if(d0_significance>3) continue;

      bool mcmatch=false;
      if(isMC)
	{
	  ElementLink< xAOD::TruthParticleContainer > truthLink = (*track_itr)->auxdata<ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");
	  if(truthLink.isValid()) 
	    {
	      mcmatch=true;
	      const xAOD::TruthParticle *tparent = (*truthLink)->parent(0);
	      if(tparent)
		{
		  if(tparent->pdgId()==Ups_ID[MC_state] && (*truthLink)->isMuon()) continue;
		}
	      ElementLink< xAOD::TruthVertexContainer > truthverLink = (*truthLink)->auxdata<ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
	      if(truthverLink.isValid())
		{
		  if(truth_primary_vx_z!=(*truthverLink)->z()) mcmatch=false;
		}
	    }
	}
      float delta_z0 = (*track_itr)->z0() + (*track_itr)->vz() - primary_vx_z;
//      cout<<"check vz = "<<(*track_itr)->vz()<<endl;
//      cout<<"check vy = "<<(*track_itr)->vy()<<endl;
//      cout<<"check vx = "<<(*track_itr)->vx()<<endl;
      //      sindeltaZdist[zposbin]->Fill(delta_z0* sin((*track_itr)->theta()), Nvtxs);
      //      if(jet4_pt->size()==1)  sindeltaZdist_onejet[zposbin]->Fill(delta_z0* sin((*track_itr)->theta()), Nvtxs);
      if(fabs(delta_z0* sin((*track_itr)->theta()))>2.) Nofbkgs++;

      Noftracks++;
      trks_true->push_back(mcmatch);
      trks_eta->push_back((*track_itr)->eta());
      trks_sinthetaz0->push_back(delta_z0* sin((*track_itr)->theta()));
      trks_phi->push_back((*track_itr)->phi());
      trks_pt ->push_back((*track_itr) ->charge()*(*track_itr) ->pt()/1000. );
    }//end of loop over tracks

  //  cout<<"finished the track stuff at aleast once"<<endl;

  if(pairpt->size()>0 || UpsID>0) pairs->Fill();
  //  else if(UpsID>0) pairs->Fill();
  //  else cout<<"Didn't fill!"<<endl; 


  recotrig1->clear();
  recotrig2->clear();
  recoutrig1->clear();
  recoutrig2->clear();
  recod01->clear();
  recod02->clear();
  recod0s1->clear();
  recod0s2->clear();
  recoiso1->clear();
  recoiso2->clear();
  recopt1->clear();
  recopt2->clear();
  recoeff1->clear();
  recoeff2->clear();

  recotosub1->clear();
  recotosub2->clear();

  recoerr1->clear();
  recoerr2->clear();

  recoeta1->clear();
  recoeta2->clear();
  recophi1->clear();
  recophi2->clear();
  recoch1->clear();
  recoch2->clear();
  pairpt->clear();
  pairy->clear();
  paireta->clear();
  pairm->clear();
  pairphi->clear();
  jet4_pt->clear();
  jet4_uncalibpt->clear();
  jet4_eta->clear();
  jet4_phi->clear();
  //  jet4_ntrks->clear();
  //  jet4_ntrks5->clear();
  //  jet4_ntrks6->clear();
  jet4_jvf->clear();
  vxs->clear();
//  jet10_pt  ->clear();
//  jet10_eta  ->clear();
//  jet10_phi  ->clear();
//  jet10_ntrks  ->clear();
//  jet10_fliptrks  ->clear();
//  jet10_jvf  ->clear();

  trks_true->clear();
  trks_eta->clear();
  trks_sinthetaz0->clear();
  trks_phi->clear();
  trks_pt->clear();

  truesib_ID->clear();
  truesib_pt->clear();
  truesib_phi->clear();
  truesib_eta->clear();
  truesib_recoq->clear();
  truesib_recoz0->clear();

  truetrk_pt->clear();
  truetrk_charge->clear();
  truetrk_vxz->clear();
  truetrk_vxx->clear();
  truetrk_vxy->clear();
  truetrk_phi->clear();
  truetrk_eta->clear();
  truetrk_recoq->clear();
  truetrk_recoz0->clear();
  //  fliptrks_pt->clear();
  //  fliptrks_eta->clear();
  //  fliptrks_phi->clear();
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once) 
  xAOD::TEvent* event = wk()->xaodEvent();
  Info("finalize()", "Number of clean events = %i", m_numCleanEvents);
  cout<<"Number of pass mass  = "<<m_passeventCounter<<endl;
  if (m_grl) {
    delete m_grl;
    m_grl = 0;
  }
  if (m_grl2) {
    delete m_grl2;
    m_grl2 = 0;
  }

  if( m_jetCleaning ) {
    delete m_jetCleaning;
    m_jetCleaning = 0;
  }

// cleaning up trigger tools
  if( m_trigConfigTool ) {
      delete m_trigConfigTool;
      m_trigConfigTool = 0;
   }
   if( m_trigDecisionTool ) {
      delete m_trigDecisionTool;
      m_trigDecisionTool = 0;
   }

   if( m_muonSelection ) {
    delete m_muonSelection;
    m_muonSelection = 0;
  }

if(m_muonCalibrationAndSmearingTool){
   delete m_muonCalibrationAndSmearingTool;
   m_muonCalibrationAndSmearingTool = 0;
}

//if(m_effi_corr){
//   delete m_effi_corr;
//   m_effi_corr = 0;
//}

 if( m_match_Tool ) {
   delete m_match_Tool;
   m_match_Tool = 0;
 }
 
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackingPerfTree :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
