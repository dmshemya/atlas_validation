#ifndef TrackingPerfTree_H
#define TrackingPerfTree_H 1


#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTrigger/TrigCompositeContainer.h"
#include "xAODTrigger/EnergySumRoI.h"


#include <string>
#include <stdlib.h>

class TrackingPerfTree: public EL::Algorithm { 
 public: 

  // utilitary variables
  std::string output_file_name;

  TrackingPerfTree();
  TrackingPerfTree(const TrackingPerfTree& alg);
  virtual ~TrackingPerfTree(); 

  virtual EL::StatusCode histInitialize();     //once, before any input is loaded
  virtual EL::StatusCode initialize();     //once, before any input is loaded
  virtual EL::StatusCode beginInputFile(); //start of each input file, only metadata loaded
  virtual EL::StatusCode execute();        //per event
  virtual EL::StatusCode finalize();       //once, after all events processed

  virtual EL::StatusCode setupJob(EL::Job& job);
  virtual EL::StatusCode changeInput(bool firstFile);
	virtual EL::StatusCode fileExecute();
	virtual EL::StatusCode postExecute();


  TTree* tree; //!

  // general
  int evt_counter = 0;
  int lb;
  int bcid;
  int run_number;

  // event cuts
  bool passedLAr;
  bool passedTile;
  bool passedSCT;
  bool passedCore;

  // vertices
  int nvertex_objects;
  int nvertex;
  int npvertex;
  std::vector<float> vertex_z; //!
  std::vector<float> primary_vertex_z; //!
  std::vector<float> distance_to_pv; //!

  // tracks
  int ntracks;
  std::vector<double> trk_pt; //!
  std::vector<double> trk_eta; //!
  std::vector<double> trk_phi; //!
  std::vector<float> trk_theta; //!
  std::vector<float> trk_d0; //!
  std::vector<float> trk_z0; //!
  std::vector<float> trk_vz; //!
  std::vector<double> trk_z0_pv; //!
  std::vector<float> trk_chi2; //!
  std::vector<float> trk_ndof; //!

  std::vector<float> trk_theta_err; //!
  std::vector<float> trk_d0_err; //!
  std::vector<float> trk_z0_err; //!

  // track hits
  std::vector<int> n_pix_hits; //!
  std::vector<int> n_pix_holes; //!
  std::vector<int> n_sh_pix_hits; //!
  std::vector<int> n_sct_hits; //!
  std::vector<int> n_sct_holes; //!
  std::vector<int> n_sh_sct_hits; //!
  std::vector<int> n_trt_hits; //!
  std::vector<int> n_ibl_hits; //!
  std::vector<int> n_bl_hits; //!
  std::vector<int> n_exp_ibl_hits; //!
  std::vector<int> n_exp_bl_hits; //!

  // this is needed to distribute the algorithm to the workers
  ClassDef(TrackingPerfTree, 1);
  
}; 

#endif //> !TrackingPerfTree_H
