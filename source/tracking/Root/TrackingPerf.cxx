#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "tracking/TrackingPerf.h"


ClassImp(TrackingPerf)

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )			\
  do {							\
    if( ! EXP.isSuccess() ) {				\
      Error( CONTEXT,					\
	     XAOD_MESSAGE( "Failed to execute: %s" ),	\
	     #EXP );					\
      return EL::StatusCode::FAILURE;			\
    }							\
  } while( false )


// this is just list of events for which we have events in bol old and new samples (updated July 22)
std::set<uint32_t> same = { 174126166,174140799,174163555,174149603,174157221,174148696,174136691,174139029,174166932,174128261,174150788,174175790,174151866,174146651,174182472,174162614,174150780,174186328,174181202,174139136,174162711,174173219,174194962,174182882,174201101,174202487,174214337,174143105,174199788,174203747,174183826,174151312,174204517,174130067,174169270,174181484,174193202,174142787,174143174,174229211,174141762,174148680,174150201,174237810,174230915,174130974,174219514,174243552,174189897,174239298,174246201,174274812,174149979,174148928,174254554,174244294,174130573,174229362,174224867,174262138,174242538,174271219,174260261,174267329,174146116,174130881,174132921,174269465,174183100,174293581,174269581,174224682,174159519,174272475,174184525,174204632,174287685,174204693,174253341,174280748,174282765,174289526,174276915,174192194,174236644,174234669,174261384,174160045,174278747,174295526,174302000,174287047,174251690,174136175,174289223,174238268,174216192,174220894,174196301,174300799,174275412,174233516,174234051,174213241,174201758,174211708,174184580,174178592,174222156,174237235,174295081,174285264,174311718,174316133,174180063,174313047,174266854,174318499,174220021,174218090,174313065,174318229,174228309,174197352,174326223,174330412,174228237,174257653,174233940,174157747,174275890,174235436,174247905,174312469,174309657,174218835,174226743,174335060,174171960,174357151,174334910,174298981,174145635,174306319,174314168,174331750,174250633,174358721,174342817,174252597,174251843,174284953,174344805,174199714,174342318,174335794,174294249,174276116,174294134,174226623,174360844,174147165,174190381,174143865,174353133,174242208,174227035,174356738,174307282,174343245,174237650,174211883,174294254,174362494,174266210,174359657,174337311,174264604,174249084,174237097,174156748,174379992,174154019,174375730,174364464,174389639,174166103,174279662,174266689,174204521,174271720,174269242,174261228,174315195,174331041,174260463,174323308,174389009,174378744,174286252,174257636,174267616,174398624,174387384,174340164,174396968,174378811,174272654,174231381,174396245,174236177,174308830,174284712,174410951,174273834,174184999,174155637,174425648,174309878,174381923,174333069,174398155,174275748,174400647,174305768,174199815,174161280,174351687,174128456,174335288,174357949,174126928,174423127,174231453,174301528,174227003,174316262,174430798,174388447,174384060,174259975,174424516,174304541,174163963,174392539,174271406,174399949,174319924,174365832,174330381,174300558,174433225,174424739,174253882,174392129,174433889,174438746,174413640,174433624,174396025,174439639,174401558,174417802,174393408,174433811,174428205,174246847,174353110,174259925,174249156,174129220,174413391,174451045,174395208,174400699,174462707,174459911,174184726,174295774,174291970,174394971,174269650,174380287,174418609,174345320,174438594,174329508,174476910,174440534,174218865,174431539,174405633,174397964,174414442,174414206,174436188,174367444,174351315,174260809,174362412,174140661,174469552,174471428,174389224,174457736,174481760,174462720,174175051,174417920,174419812,174411634,174472096,174243569,174197451,174446281,174336554,174439789,174412806,174322708,174349142,174420713,174356747,174517594,174356987,174128268,174167913,174418314,174459937,174412019,174483667,174445902,174355785,174337976,174399363,174412548,174362275,174403498,174494618,174509184,174303131,174293102,174475835,174431486,174536745,174427221,174358932,174497467,174513051,174408273,174377801,174508830,174132816,174532027,174440225,174450951,174502816,174473401,174534558,174229336,174496469,174179613,174476326,174394777,174397334,174498467,174529628,174404861,174433092,174524044,174399373,174402436,174486343,174525167,174510343,174284351,174463036,174405323,174525402,174467281,174262142,174463160,174171946,174500625,174377337,174478479,174375544,174463683,174311262,174286212,174549804,174548130,174528803,174550395,174506985,174502080,174475957,174505234,174555069,174377831,174501173,174498426,174374791,174477944,174476304,174379031,174510448,174476725,174562357,174569012,174570747,174453391,174528710,174529090,174406399,174489985,174430156,174570492,174426579,174537179,174233178,174517865,174550138,174505630,174579681,174544109,174541317,174507528,174463954,174483899,174577690,174476579,174279692,174557857,174579333,174228732,174580017,174437449,174570663,174327435,174588212,174436025,174584730,174589705,174348268,174236922,174533998,174469781,174541862,174588559,174386681,174500949,174554373,174586757,174320262,174610134,174532218,174595546,174555013,174597284,174340792,174597487,174211494,174592256,174598753,174130383,174409515,174511992,174493386,174390048,174567361,174507852,174601708,174585450,174518365,174384820,174611715,174582840,174592094,174641719,174606050,174571019,174457550,174618611,174235722,174588888,174511664,174616163,174502540,174578884,174602711,174336169,174293540,174392322,174573396,174642786,174650927,174483181,174564078,174598534,174610074,174551923,174258556,174379941,174561183,174603403,174458487,174582831,174538793,174574710,174544952,174439857,174418402,174639935,174426892,174575617,174289007,174419119,174507034,174627573,174563340,174651345,174650614,174618361,174521819,174582739,174355421,174640086,174208003,174580842,174402065,174626423,174631076,174594064,174228521,174484373,174629169,174359176,174605871,174559770,174367389,174516722,174529417,174667241,174621900,174637318,174286912,174400712,174525479,174617784,174647173,174237192,174578764,174424168,174584127,174620940,174665786,174607704,174264651,174611025,174533451,174589280,174668065,174640400,174511625,174692100,174655430,174477352,174388518,174608529,174567062,174668055,174570809,174635981,174138019,174674188,174158836,174331385,174654559,174630689,174639235,174479145,174216796,174427722,174619644,174680068,174238836,174600663,174594742,174670271,174670260,174229570,174686548,174489043,174632987,174604958,174609613,174188335,174621276,174429552,174599251,174686408,174502610,174570501,174677229,174715363,174563583,174698892,174685476,174685050,174632947,174698309,174567765,174674791,174703516,174555137,174469223,174629031,174707460,174481395,174643260,174740501,174629466,174636237,174466453,174211897,174532923,174553786,174711731,174690594,174717753,174393200,174616128,174662701,174677661,174424043,174712462,174458019,174547233,174478879,174656751,174597207,174620982,174723595,174669580,174672942,174676448,174709644,174725717,174587747,174540434,174712403,174644815,174719304,174663772,174399302,174738400,174543331,174161121,174722765,174658132,174748695,174711943,174670551,174760552,174644340,174700597,174718691,174664003,174669393,174535098,174477793,174719550,174747782,174711376,174740883,174664864,174750332,174749424,174601637,174554111,174461068,174707007,174689190,174482951,174716556,174437918,174664046,174757894,174748526,174709390,174767015,174672600,174556671,174760450,174603578,174774817,174759197,174707646,174743976,174753779,174657042,174558139,174671194,174607333,174678110,174320629,174790781,174636390,174760771,174772782,174513506,174524417,174571162,174565602,174645019,174271461,174642360,174736082,174761206,174758479,174679176,174693287,174739156,174695881,174787567,174701149,174738097,174573600,174769436,174750142,174799746,174648987,174709173,174712439,174639790,174697492,174726870,174773389,174476158,174630002,174694685,174765662,174760668,174702483,174775873,174709200,174793401,174775400,174747204,174786805,174709011,174785564,174735473,174683157,174708865,174780285,174305073,174530227,174720161,174738420,174627892,174796708,174760984,174803769,174637455,174768719,174644475,174742648,174707491,174810060,174767370,174821841,174543445,174646204,174779769,174805914,174751500,174751761,174778924,174760149,174838284,174663521,174514631,174690006,174788122,174503087,174646559,174747181,174846201,174804069,174685959,174490732,174706623,174624208,174824707,174548848,174684967,174703399,174299300,174823002,174599634,174475764,174831926,174607878,174828782,174813467,174767307,174694577,174762288,174798518,174780606,174795876,174639549,174372611,174814011,174801670,174813685,174464836,174837749,174757343,174786833,174590763,174647801,174810661,174819560,174775780,174568171,174304219,174771090,174853432,174711242,174251434,174580272,174818298,174856572,174854921,174634586,174849128,174740390,174788760,174688766,174808494,174626959,174776632,174716504,174624464,174667679,174820259,174873592,174860026,174736053,174778818,174858126,174797846,174887127,174862667,174842322,174813963,174835068,174760245,174781299,174876103,174758870,174894634,174819565,174513489,174819192,174539647,174878570,174202396,174872536,174216243,174880192,174629318,174652643,174186009,174806444,174906693,174817516,174769103,174801292,174740246,174804829,174796694,174674410,174891179,174803471,174886649,174695769,174684884,174695650,174880167,174884885,174830176,174897294,174912206,174852185,174897820,174649147,174902967,174795052,174575262,174868142,174830608,174655340,174216425,174211026,174870650,174854345,174688045,174763121,174901894,174601050,174866289,174874924,174558065,174866427,174882010,174644913,174886605,174892882,174816361,174948499,174903941,174581898,174554485,174868705,174872834,174925209,174514626,174877924,174928457,174868494,174171148,174698552,174912193,174669159,174887077,174916019,174921144,174911813,174339599,174740646,174875035,174828271,174832725,174916592,174386893,174299027,174952846,174851539,174659126,174386832,174406953,174952486,174813605,174897459,174799045,174224685,174799114,174834435,174808501,174731253,174979317,174916710,174960182,174849326,174974510,174985483,174888835,174900364,174904527,174606506,174841593,174956850,174881473,174793568,174918391,174847276,174806964,174795202,174847417,174858779,174819206,174936634,174930462,174776477,174877177,174931121,174990916,174963133,174810031,174828975,174686648,174973555,174987909,174848030,174378788,175012358,174904281,174316614,174987355,174925194,174856696,174932393,174831885,174922787,174847550,174876544,174982160,174996361,174864147,174984133,174856867,174783917,174960473,174982130,174938723,174931415,174972300,174905879,174983923,174883481,174991247,174884267,174668189,174930661,174803343,175009938,175013429,174941561,174706606,174911129,175000982,174915521,174868546,174873914,174968324,174992934,174802689,174986202,174949051,174946456,174958289,174978807,174989978,174796630,174818142,174952940,175007898,174823530,174669004,174878159,174951340,174983964,174966165,174979972,174890983,174937421,174938684,174891332,174874436,174956837,175008079,174965996,174785828,174422304,174198288,174796408,175042061,175019290,175028437,175042367,174377748,174868617,175010782,174956248,174835511,175034219,175020236,175050972,174848936,174944340,174981280,174367583,174524985,174762948,174964044,174965060,174697126,174694028,174574491,175005073,174965855,174703470,174823416,175043527,175037216,174560384,174901451,175047614,174998256,174935604,175051258,174751684,175057582,174885471,174988411,175002435,174928862,175041385,174999923,174988260,174812909,174743076,174886959,174691099,175057125,174973791,174943987,174996551,175020739,175048801,175069962,174884801,174579183,174964257,174835782,174938781,175025981,174784912,174908579,174403234,174134329,175063716,174999470,175029959,175055199,174653765,175091868,174699967,174964816,174774811,174753173,174997026,174913966,174957202,174997381,174839590,174939457,175036105,174491706,174931970,174734040,175029276,174667409,174982326,174586018,175093921,174699365,174917945,175097966,174769178,175028045,175051843,175098967,175023721,175075708,175029950,174875194,174747569,175032252,174983939,175133083,175129241,174856214,175100557,174970006,174918471,174576628,175030696,174705504,175094257,175021195,175120016,175124565,175134979,175039451,175025746,175114367,174962882,175064679,174824667};

TrackingPerf::TrackingPerf()
{
}

TrackingPerf::TrackingPerf(const TrackingPerf& alg)
{
}


TrackingPerf::~TrackingPerf() {}

std::string add_pref(std::string pref, std::string str)
{
  std::string result = pref + str;
  return result.c_str();
}

EL::StatusCode TrackingPerf::setupJob(EL::Job& job)
{
	// let's initialize the algorithm to use the xAODRootAccess package
	job.useXAOD();
	
	EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
	std::cout << " Job setup done!" << std::endl;
	
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerf::fileExecute()
{
	// Here you do everything that needs to be done exactly once for every
	// single file, e.g. collect a list of all lumi-blocks processed
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerf::postExecute()
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerf::changeInput(bool firstFile)
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerf::initialize()
{
  std::string message = "Initializing " + name() + "...";
  Info("initialize()", message.c_str());

	xAOD::TEvent* event = wk()->xaodEvent();
	Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  // general
  book(TH1D("counter", "ev count", 10, 0, 10));
  book(TH1D("lb", ";lb number", 1000, -0.5, 1000-0.5));
  book(TH1D("bcid", ";bcid number", 4000, -0.5, 4000-0.5));

  // vertices
  book(TH1D("nvertex_objects", ";count", 20, -0.5, 20-0.5));
  book(TH1D("nvertex", ";count", 20, -0.5, 20-0.5));
  book(TH1D("nvertex", ";count", 20, -0.5, 20-0.5));
  book(TH1D("vertex_z", ";z[mm]", 400, -200, 200));
  book(TH2D("lb_vertex_z", ";lb;vz [mm]", 1000, -0.5, 999.5, 400, -200, 200));
  book(TH1D("primary_vertex_z", ";z_{pv}[mm]", 400, -200, 200));
  book(TH1D("distance_to_pv", ";z[mm]", 400, -200, 200));
  book(TH2D("distance_to_pv_corr", ";z_{pv}[mm];z_{other}[mm]", 400, -200, 200, 400, -200, 200));

  // tracks
  book(TH1D("ntracks", ";n", 1000, 0, 12000));
  book(TH1D("pt_zoom", ";pt[GeV]", 200, 0, 1.1));
  book(TH1D("pt", ";pt[GeV]", 200, 0, 10));
  book(TH1D("pt_wide", ";pt[GeV]", 200, 0, 100));
  book(TH1D("eta", ";#eta", 100, -2.5, 2.5));
  book(TH1D("phi", ";#phi", 64, -M_PI, M_PI));
  book(TH1D("sin_theta", ";sin(#theta)", 100, 0, 1));
  book(TH1D("d0", ";d_{0}", 200, -10, 10));
  book(TH1D("z0", ";z_{0}", 400, -200, 200));
  book(TH1D("z0_sin_theta", ";z_{0} * sin(#theta)", 400, -200, 200));
  book(TH1D("z0_zoom", ";z_{0}", 200, -10, 10));
  book(TH1D("z0_zoom_sin_theta", ";z_{0} * sin(#theta)", 200, -10, 10));
  book(TH1D("vz", ";vz", 400, -10, 10));
  book(TH2D("lb_vx", ";lb;vx [mm]", 1000, -0.5, 999.5, 400, -10, 10));
  book(TH2D("lb_vy", ";lb;vy [mm]", 1000, -0.5, 999.5, 400, -10, 10));
  book(TH2D("lb_vz", ";lb;vz [mm]", 1000, -0.5, 999.5, 400, -10, 10));
  book(TH1D("z0_pv", ";z_{0}", 400, -200, 200));
  book(TH1D("z0_pv_zoomed", ";z_{0}", 200, -10, 10));
  book(TH1D("z0_pv_sin_theta", ";z_{0} * sin(#theta)", 400, -200, 200));
  book(TH1D("z0_pv_zoomed_sin_theta", ";z_{0} * sin(#theta)", 200, -10, 10));
  book(TH1D("chi2", ";chi2", 200, 0, 100));
  book(TH1D("ndof", ";ndof", 50, 0, 50));
  book(TH1D("chi2_over_ndof", ";chi2/ndof", 500, 0, 50));

  // track hits
  int n_eta_bins = 50;
  book(TH2D("eta_n_pix_hits", ";#eta;n_pix_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  book(TH2D("eta_n_pix_holes", ";#eta;n_pix_holes", n_eta_bins, -2.5, 2.5, 10, 0-0.5, 10-0.5));
  book(TH2D("eta_n_sh_pix_hits", ";#eta;n_sh_pix_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  book(TH2D("eta_n_sct_hits", ";#eta;n_sct_hits", n_eta_bins, -2.5, 2.5, 100, 0-0.5, 100-0.5));
  book(TH2D("eta_n_sct_holes", ";#eta;n_sct_holes", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  book(TH2D("eta_n_sh_sct_hits", ";#eta;n_sh_sct_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  book(TH2D("eta_n_trt_hits", ";#eta;n_trt_hits", n_eta_bins, -2.5, 2.5, 100, 0-0.5, 100-0.5));
  book(TH2D("eta_n_ibl_hits", ";#eta;n_ibl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  book(TH2D("eta_n_bl_hits", ";#eta;n_bl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  book(TH2D("eta_n_exp_ibl_hits", ";#eta;n_exp_ibl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  book(TH2D("eta_n_exp_bl_hits", ";#eta;n_exp_bl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));

  // in multiplicities
  std::vector<int> multiplicities({50, 100, 500, 1000, 2000, 3000, 4000});
  for (const auto& mult : multiplicities)
  {
    std::string mult_str = "mult" + std::to_string(mult) + "_";
    book(TH2D(add_pref(mult_str, "eta_n_pix_hits").c_str(), ";#eta;n_pix_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_pix_holes").c_str(), ";#eta;n_pix_holes", n_eta_bins, -2.5, 2.5, 10, 0-0.5, 10-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_sh_pix_hits").c_str(), ";#eta;n_sh_pix_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_sct_hits").c_str(), ";#eta;n_sct_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_sct_holes").c_str(), ";#eta;n_sct_holes", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_sh_sct_hits").c_str(), ";#eta;n_sh_sct_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_trt_hits").c_str(), ";#eta;n_trt_hits", n_eta_bins, -2.5, 2.5, 100, 0-0.5, 100-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_ibl_hits").c_str(), ";#eta;n_ibl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_bl_hits").c_str(), ";#eta;n_bl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_exp_ibl_hits").c_str(), ";#eta;n_exp_ibl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
    book(TH2D(add_pref(mult_str, "eta_n_exp_bl_hits").c_str(), ";#eta;n_exp_bl_hits", n_eta_bins, -2.5, 2.5, 30, 0-0.5, 30-0.5));
  }

  return StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerf::finalize()
{
  std::string message = "Finalizing " + name() + "...";
  Info("finalize()", message.c_str());

  return StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerf::execute()
{
  std::string message = "Executing " + name() + "...";
  Info("execute()", message.c_str());
  hist("counter")->Fill(0.5); // all

  // getting event info
  const xAOD::EventInfo* ptrEvt = 0;
  EL_RETURN_CHECK("execute()", evtStore()->retrieve(ptrEvt, "EventInfo"));
  message = "EventNo: " + std::to_string(ptrEvt->eventNumber());
  Info("execute()", message.c_str());

  // passing detector parts without errors
  bool isPassErrState = true;
  if (ptrEvt->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) isPassErrState = false;
  if (ptrEvt->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) isPassErrState = false;
  if (ptrEvt->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) isPassErrState = false;
  if (ptrEvt->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) isPassErrState = false;
  if (!isPassErrState)
    return StatusCode::SUCCESS;
  hist("counter")->Fill(1.5); // passed
  hist("lb")->Fill(ptrEvt->lumiBlock());
  hist("bcid")->Fill(ptrEvt->bcid());

  // getting all vertices
  const xAOD::VertexContainer* vertices = nullptr;
  EL_RETURN_CHECK("execute()", evtStore()->retrieve(vertices, "PrimaryVertices"));
  std::vector<const xAOD::Vertex*> allvertices(vertices->begin(), vertices->end());
  hist("nvertex_objects")->Fill(allvertices.size());

  // getting all but NoVtx type vertices
  std::vector<const xAOD::Vertex*> fvertices;
  for (const xAOD::Vertex* vtx : allvertices)
    if (vtx && vtx->vertexType() != xAOD::VxType::NoVtx)
      fvertices.push_back(vtx);
  hist("nvertex")->Fill(fvertices.size());

  // filling z of vertices and the primary vertex
  std::unique_ptr<xAOD::Vertex> pv;
  for (const xAOD::Vertex* vtx : fvertices)
  {
    hist("vertex_z")->Fill(vtx->z());
    hist("lb_vertex_z")->Fill(ptrEvt->lumiBlock(), vtx->z());
    if (vtx->vertexType() == xAOD::VxType::PriVtx)
    {
      hist("primary_vertex_z")->Fill(vtx->z());
      pv = std::make_unique<xAOD::Vertex>(*vtx);
    }
  }

  // filling distances to the primary vertex
  if (pv) 
  {
    for (const xAOD::Vertex* vtx : fvertices)
    {
      if (vtx->vertexType() != xAOD::VxType::PriVtx)
      {
        hist("distance_to_pv")->Fill(vtx->z() - pv->z());
        hist("distance_to_pv_corr")->Fill(vtx->z(), pv->z());
      }
    }
  }

  // getting all tracks
  const xAOD::TrackParticleContainer* tracks = nullptr;
  EL_RETURN_CHECK("execute()", evtStore()->retrieve(tracks, "InDetTrackParticles"));
  std::vector<const xAOD::TrackParticle*> ftracks(tracks->begin(), tracks->end());
  const int tracks_size = ftracks.size();
  hist("ntracks")->Fill(tracks_size);

  // iterating over all tracks
  for (const xAOD::TrackParticle* track: ftracks)
  {
    // filling basic track parameters
    hist("pt")->Fill(track->pt() * 1e-3);
    hist("pt_zoom")->Fill(track->pt() * 1e-3);
    hist("pt_wide")->Fill(track->pt() * 1e-3);
    hist("eta")->Fill(track->eta());
    hist("phi")->Fill(track->phi());
    hist("sin_theta")->Fill(sin(track->theta()));
    hist("d0")->Fill(track->d0());
    hist("z0")->Fill(track->z0());
    hist("z0_zoom")->Fill(track->z0());
    hist("z0_sin_theta")->Fill(track->z0() * sin(track->theta()));
    hist("z0_zoom_sin_theta")->Fill(track->z0() * sin(track->theta()));
    hist("vz")->Fill(track->vz());
    hist("lb_vx")->Fill(ptrEvt->lumiBlock(), track->vx());
    hist("lb_vy")->Fill(ptrEvt->lumiBlock(), track->vy());
    hist("lb_vz")->Fill(ptrEvt->lumiBlock(), track->vz());
    if (pv) 
    {
      double z0_pv = track->z0() + track->vz() - pv->z();
      hist("z0_pv")->Fill(z0_pv);
      hist("z0_pv_zoomed")->Fill(z0_pv);
      hist("z0_pv_sin_theta")->Fill(z0_pv * sin(track->theta()));
      hist("z0_pv_zoomed_sin_theta")->Fill(z0_pv * sin(track->theta()));
    }
    hist("chi2")->Fill(track->chiSquared());
    hist("ndof")->Fill(track->numberDoF());
    hist("chi2_over_ndof")->Fill(track->chiSquared() / track->numberDoF());

    // filling hits and holes
    int trkNumberOfPixelHits = track->auxdata<unsigned char>("numberOfPixelHits") + track->auxdata<unsigned char>("numberOfPixelDeadSensors");
    int trkNumberOfPixelHoles = track->auxdata<unsigned char>("numberOfPixelHoles");
    int trkNumberOfPixelSharedHits = track->auxdata<unsigned char>("numberOfPixelSharedHits");
    int trkNumberOfSCTHits = track->auxdata<unsigned char>("numberOfSCTHits") + track->auxdata<unsigned char>("numberOfSCTDeadSensors");
    int trkNumberOfSCTHoles = track->auxdata<unsigned char>("numberOfSCTHoles");
    int trkNumberOfSCTSharedHits = track->auxdata<unsigned char>("numberOfSCTSharedHits");
    int trkNumberOfTRTHits = track->auxdata<unsigned char>("numberOfTRTHits");
    int trkNumberOfInnermostPixelLayerHits = track->auxdata<unsigned char>("numberOfInnermostPixelLayerHits");
    int trkNumberOfNextToInnermostPixelLayerHits = track->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerHits");
    int trkExpectInnermostPixelLayerHit = track->auxdata<unsigned char>("expectInnermostPixelLayerHit");
    int trkExpectNextToInnermostPixelLayerHit = track->auxdata<unsigned char>("expectNextToInnermostPixelLayerHit");

    hist("eta_n_pix_hits")->Fill(track->eta(), trkNumberOfPixelHits);
    hist("eta_n_pix_holes")->Fill(track->eta(), trkNumberOfPixelHoles);
    hist("eta_n_sh_pix_hits")->Fill(track->eta(), trkNumberOfPixelSharedHits);
    hist("eta_n_sct_hits")->Fill(track->eta(), trkNumberOfSCTHits);
    hist("eta_n_sct_holes")->Fill(track->eta(), trkNumberOfSCTHoles);
    hist("eta_n_sh_sct_hits")->Fill(track->eta(), trkNumberOfSCTSharedHits);
    hist("eta_n_trt_hits")->Fill(track->eta(), trkNumberOfTRTHits);
    hist("eta_n_ibl_hits")->Fill(track->eta(), trkNumberOfInnermostPixelLayerHits);
    hist("eta_n_bl_hits")->Fill(track->eta(), trkNumberOfNextToInnermostPixelLayerHits);
    hist("eta_n_exp_ibl_hits")->Fill(track->eta(), trkExpectInnermostPixelLayerHit);
    hist("eta_n_exp_bl_hits")->Fill(track->eta(), trkExpectNextToInnermostPixelLayerHit);

    std::vector<int> multiplicities({ 50, 100, 500, 1000, 2000, 3000, 4000 });
    for (const auto& mult : multiplicities)
    {
      if (mult - 50 < tracks_size and tracks_size < mult + 50) {
        std::string mult_str = "mult" + std::to_string(mult) + "_";
        hist(add_pref(mult_str, "eta_n_pix_hits").c_str())->Fill(track->eta(), trkNumberOfPixelHits);
        hist(add_pref(mult_str, "eta_n_pix_holes").c_str())->Fill(track->eta(), trkNumberOfPixelHoles);
        hist(add_pref(mult_str, "eta_n_sh_pix_hits").c_str())->Fill(track->eta(), trkNumberOfPixelSharedHits);
        hist(add_pref(mult_str, "eta_n_sct_hits").c_str())->Fill(track->eta(), trkNumberOfSCTHits);
        hist(add_pref(mult_str, "eta_n_sct_holes").c_str())->Fill(track->eta(), trkNumberOfSCTHoles);
        hist(add_pref(mult_str, "eta_n_sh_sct_hits").c_str())->Fill(track->eta(), trkNumberOfSCTSharedHits);
        hist(add_pref(mult_str, "eta_n_trt_hits").c_str())->Fill(track->eta(), trkNumberOfTRTHits);
        hist(add_pref(mult_str, "eta_n_ibl_hits").c_str())->Fill(track->eta(), trkNumberOfInnermostPixelLayerHits);
        hist(add_pref(mult_str, "eta_n_bl_hits").c_str())->Fill(track->eta(), trkNumberOfNextToInnermostPixelLayerHits);
        hist(add_pref(mult_str, "eta_n_exp_ibl_hits").c_str())->Fill(track->eta(), trkExpectInnermostPixelLayerHit);
        hist(add_pref(mult_str, "eta_n_exp_bl_hits").c_str())->Fill(track->eta(), trkExpectNextToInnermostPixelLayerHit);
      }
    }
  }

  return StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerf::beginInputFile()
{
  return StatusCode::SUCCESS;
}
