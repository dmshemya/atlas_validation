
#for local test
#data_names=("data18_hi.00367134.physics_HardProbes.merge.AOD.f1030_m2048")

#for grid test
data_names=("data18_hi.00367134.physics_HardProbes.merge.AOD.f1030_m2048"
    "data18_hi.00367134.physics_HardProbes.merge.AOD.r14821_p5760")
source build.sh

for dataset in "${data_names[@]}"
do

    output="user.sbaidyan.validation_${dataset}_07092023" # change the last no. for different jobs
    input="$dataset"	
    
    set -x
    run_tracking -c myConfig_grid.cfg -i $input -o $output
    set +x
done
