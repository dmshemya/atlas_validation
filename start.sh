#showVersions athena | grep AnalysisBase
#asetup --restore ->to use same asetup

setupATLAS
lsetup panda
rm -rf build
mkdir build
cd build
asetup AnalysisBase,24.2.0,here
cmake ../source/
source x86_64*/setup.sh
make
cd ..
